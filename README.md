# Dioptra Transaction Data Pipeline

## Development Setup

You'll need Docker, that's it. No local Python development environment needed.

## Development Tasks

```
build-deploy-image             Build the docker deployment container image.
build                          Build the docker development container image.
cfn-lint                       Run the cfn-lint linter on the CloudFormation templates.
create-bastion-key-pair        Create an RSA key pair for accessing a bastion host. Run this once in each AWS account.
create-ecr-repo                Create ECR repo for dioptra/transaction-pipeline. Run this once in each AWS account.
create-s3-base-bucket          Create S3 base bucket. Run this once in each AWS account.
delete-cf-stack                [STACK][DANGEROUS] Delete the cloudformation STACK. Requires DANGEROUS in env.
deploy-cf-stack                [APP_VERSION][STACK] Deploy (create or update) the cloudformation STACK from local template.
deploy                         [APP_VERSION][STACK] Build and deploy a new APP_VERSION to STACK.
down                           Destroy the docker-compose services.
emplace-pipeline-config        [STACK] Push a valid config.yml file to the S3 bucket for the STACK.
emplace-test-transactions      Upload sample transaction zip files in temp dir to S3.
fmt                            Format the source code with black.
get-bastion-ip                 [STACK] Get the IP of the stack's bastion host
get-db-password                [STACK] Get the DB password for the current stack
get-key-path                   Get the path to the SSH key for the current AWS account and region
init-cf-prereqs                [STACK] Take care of prerequisites before we can create STACK, like emplacing required SSM config.
lint                           Run the flake8 linter on the codebase.
logs                           [STACK] Tail CloudWatch logs for the STACK.
package-cf-stack               [STACK] Package the cloudformation template for the STACK.
psql                           Launch psql, pointing at the development database.
push-image                     [APP_VERSION] Push the local dioptra image to ECR with a tag of APP_VERSION.
put-ssm-param                  [STACK][KEY][VALUE] Set KEY=VALUE in SSM.
shell-test                     Launch a shell in the development container, configured for the test database.
shell                          Launch a shell in the development container.
ssh-bastion                    [STACK] SSH to the stack's bastion host
ssh-pg-tunnel-bastion          [STACK] Open a tunnel from local port 5433 to the DB cluster port 5432 via the bastion host
stop                           Stop the docker-compose services.
test                           Run the test suite.
testfullimport                 Run the full import pipeline against the development database.
testimport-100k                Run a 100k import pipeline against the test database.
testimport-10m                 Run a 10M import pipeline against the test database.
testimport-1                   Run a single-record import pipeline against the test database.
testimport-1m                  Run a 1M import pipeline against the test database.
testimport-30m                 Run a 30M import pipeline against the test database.
triggerimport-100k             [STACK] Trigger remote import of 100k row s3 file (see emplace-test-transactions).
triggerimport-10m              [STACK] Trigger remote import of 10 million row s3 file (see emplace-test-transactions).
triggerimport-1                [STACK] Trigger remote import of 1 row s3 file (see emplace-test-transactions).
triggerimport-1m               [STACK] Trigger remote import of 1 million row s3 file (see emplace-test-transactions).
triggerimport-30m              [STACK] Trigger remote import of 30 million row s3 file (see emplace-test-transactions).
up                             Start the docker-compose services.
update-cf-stack                [APP_VERSION][STACK] Package local template and deploy the cloudformation STACK.
update-stack-app-version       [APP_VERSION][STACK] Update the cloudformation STACK using existing remote template.
```

## Developer Workflow

### Quickstart

```bash
make build   # Build the development container
make up      # Start the Compose environment
make test    # Run unit tests
make lint    # Run linter
```

#### Development Container and Dependency Management

All development for this project is performed inside ephemeral Docker containers built from the `Dockerfile`.

First, build the development container image with `make build`.
You'll need to rebuild the container image any time you modify package dependencies in `/requirements`,
but not when you've made code changes.

If you want a shell into the development container, you can `make shell`.
The shell will launch in the root directory of this project with the source files volume-mounted from your host.
The container has a pypy 3.6 runtime with all application dependencies installed system-wide via pip.
You can run `pypy3` commands manually from here, or a debugger, or whatever you want.

#### Configuration

Local development configuration goes in a `.env` file in the root of the repo (`KEY=value` format),
which is gitignored. See `appconfig/__init__.py` for available configuration options and defaults.
The app should be runnable locally with the default configuration.
Configuration overrides must live in `.env`,
as your shell's environment variables are not propagated through to the container.

#### Database Management

The application requires a PostgreSQL database.
This repo comes with Dockerized PostgreSQL development and test servers,
created as a Docker Compose environment defined in the `docker-compose.yml`.
The app is configured to use these database servers by default.

Start the compose environment with `make up`.
The servers will stay running in the background until you `make stop`,
which you don't need to ever do unless you want to.
The servers store data on persistent volumes,
so it's safe to stop and start the Compose environment at will with no data loss.
If you want to destroy all existing data and start fresh,
you can `make down` to destroy the Compose environment,
and then `make up` again to start new database servers.

If you want to run your own PostgreSQL server,
you can add a `DATABASE_URL` configuration value in your `.env`.

## Code Formatting and Linting

Code is linted with `flake8` via the `make lint` task.

Formatting is done with Black via `make fmt`.

## Testing

There are four different levels of testing:

- Unit tests
- Importer tests of a local file
- Pipeline tests of files in S3
- Full integration tests

### Unit Tests

Batteries included!

    $ make up
    $ make test

### Importer Tests

You'll run these when you're doing performance tuning or testing of the importer itself.

These require the input `.csv.zip` file.
Then you can run the `dioptra.importer` module:

    $ dotenv run python -m dioptra.importer temp/transactions.100K.csv.zip

We use `dotenv` to load your `.env` file.
This is a totally local process and just requires your Postgres service running,
and the input zip file.

There are several Make commands that sould work as long as you have files in the right place:

    $ make testimport-1
    $ make testimport-100k
    $ make testimport-1m
    $ make testimport-10m
    $ make testimport-30m

### Pipeline Tests

These run your local code against remote resources (config and zip archive).

Run these when you're verifying the entire system works front-to-back.
Remember that going from S3 to your computer is very slow
compared to everything in the datacenter, so the additional S3 download slowness
is not representative.

They require AWS S3 access to get the config and zip files.
You'll need to make sure you have the right config vars in your environment
(I recommend using `AWS_PROFILE` rather than using the access key variables directly!).
Put then in your `.env` file. Then you can run:

    $ dotenv run python -m dioptra.pipeline

There's also a Make command that should work given the files in the right place:

    $ make testfullimport

Remember that you can put files in different places and use config to point at
a different file (ie, so you can have all your test files uploaded into S3
and choose which one you want to run with):

    $ S3_ARCHIVE_KEY=invalid-transactions.csv.zip make testfullimport
    $ S3_CONFIG_KEY=invalid-config.yml make testfullimport

### Full Integration Tests

Runs the entire pipeline by uploading a file to S3 to trigger the ECS task.
Useful to verify the entire system is working (like if you add new config)
but generally not useful for development otherwise.

To do this, you will need an environment set up, as per "Deployment" below
(if those instructions are still correct- as of 4/24 they were no longer usable
due to infrastructure changes).
Then you can run:

```
$ make emplace-pipeline-config
$ make emplace-test-transactions
$ make triggerimport-100k
$ make logs
```

## Pipeline Design

The high-level design of the pipeline is:

- Users upload a `transactions.csv.zip` file to an S3 bucket.
- S3 PUT trigger triggers an ECS task (the task does not need parameters since we always use the same filename).
- ECS task pulls down the zip file, unzips it, reads and processes each row in the CSV.
- We validate the rows, and import the data into a new Postgres table.
- If we see 50 or more validation errors, or hit an exception,
  we abandon the import immediately and email a list of users (emails are sent via SES).
- If there are any validation errors, we don't modify the live data, and email a list of users.
- If we're all good, drop the old table, rename the new table, and email the users about the successful import.

This is described in some Google docs you should have access to.

### Code Design

There are three components to the system:

- `importer` takes a `.csv.zip` file and does the validation and inserting.
- `notifier` handles the emailing.
- `pipeline` glues together the entrypoint, importing, and notifications.

Most of the complexity is in the importer, and generally that complexity is around
parallelizing the validation and inserting.
The other modules should be understandable at a glance.

Extensive notes about performance and optimizations follow.

### Performance Notes

Most things about the architecture have been chosen based on performance over alternatives.
Those choices and concerns are documented here, though also refer to Git history for a fuller picture.

- PyPy is used because it is much faster than CPython.
- Reading and iterating the CSV is done in the main thread, and the rest of the work is farmed out to a threadpool.
- Thread pools are used because the cost of pickling and IPC is far higher
  than the savings due to multicore.
- About 30% of the processing cost is in Python; Postgres inserts are the bottleneck.
- COPY FROM is about twice as fast as multi-value inserts.
- A chunk size of reading and processing (validating and COPY FROM inserting) 100,000 lines at a time
  seems to be optimal. With multi-value inserts, 5,000 lines was much faster.
  A lower chunk size should in theory be used earlier on, so we can start inserting faster.
  In practice, this was not useful.
- Increasing threads higher didn't seem to help.
- The code currently reads lines, then joins them together for the COPY FROM.
  This was much faster than the alternative (reading big chunks, and iterating over it for CSV row parsing).
  It's possible I did this incorrectly. It's possible rewriting this entirely
  with custom CSV parsing that can work without splitting lines, or some other lower-level work,
  would speed things up.
- Using UNLOGGED tables resulted in 15% faster inserts.
  If we ALTER TABLE SET LOGGED after inserts, we pay the price- so we keep the table UNLOGGED.
  If it gets truncated due to a crash, we can regenerate it.
- Counting rows is really slow.
  We store an additional meta table that keeps track of information about the processing.
- Streaming the file from S3 requires a seekable file object, not just one that can read incrementally,
  so the default S3 object stream object doesn't work.
  This ends up making many calls to S3 using the Range header to get parts of the file,
  and reading those synchronously.
  This is 30-50% slower than downloading the entire file, and decompressing and parsing it in memory.

## Errors and Exceptions

There are a couple different types of 'categories' of errors.

The first are _pipeline_ errors. These should be unusual, usually due to a malformed config,
missing archive, etc. The process will error, and it will be handled by whatever top-level
process failure handler is set up, like Sentry.

The other are _importer_ errors. These will be common- some of them are handled/expected,
but even for unhandled ones, we'll report them to Sentry and then email the user.
These are generally more common and usually actionable.

In theory, we would probably want to email users even for pipeline errors, not just importer errors.
In practice, this is difficult, since the machinery for emailing users
requires a significant amount of setup (like pulling down the config!).
So we just settle on telling users about import errors,
and (at least for now) handling pipeline errors through Sentry.

## Deployment

This application is deployed to AWS via CloudFormation.

### Prerequisites

AWS environment variables for your target account and region must be present in your shell environment or in the `.env` file:

- `AWS_DEFAULT_REGION`
- `AWS_ACCESS_KEY_ID`
- `AWS_SECRET_ACCESS_KEY`

### AWS Account Setup

For each new AWS account that you wish to deploy this app to,
you must set up a base S3 bucket for CloudFormation and sample transactions, and an ECR repository for container images.
These resources are shared across all application stacks in that account.
There are `make` tasks for creating these resources:

    $ make create-s3-base-bucket
    $ make create-ecr-repo

You'll also need EC2 keypairs (to access bastion hosts) in each region in which you'll deploy this app. Run the following task to make a keypair for the current region:

    $ make create-bastion-key-pair

This command will generate a keypair for the current AWS account and region, and save the private key in `~/.ssh/dioptra-bastion-<AWS region>-<AWS account>`. You'll use this key when SSHing to the bastion host.

### Building and Pushing Container Images

Once you have an ECR repository in your target AWS account,
you can build and push a container image tagged with a version identifier:

    $ make build-deploy-image                # Build the dioptra/transaction-pipeline container image.
    $ APP_VERSION=1.0 make push-image        # Push the dioptra/transaction-pipeline:1.0 image tag to ECR.

You must provide `APP_VERSION` to the `push-image` task to specify your new version.

### Stack Creation

"Stacks" (deployed environments/instances of this application)
are identified via their CloudFormation StackName,
provided via the `STACK` environment variable to various `make` commands.
There is no separate "environment" identifier.

Note that all of the commands here require `STACK` set- the Make file will complain if it's not.
You can also refer to the Makefile if you're unclear whether a command requires a Stack or not
(though basically everything except creating the CF bucket or involving the ECR repo requires it).

For brevity (and ease of copy/paste), go ahead and export `STACK` to the name of your stack.
We'll name ours "dioptra-qa".

> NOTE: Prefix your stack with "dev" to ignore it in the `/cloudformation/built` folder.

    $ export STACK=dioptra-qa

First emplace some required SSM parameters by running the `init-cf-prereqs` task:

    $ make init-cf-prereqs

You will need to run `init-cf-prereqs` just once, when creating a new stack.
Once that's done, you can package the CloudFormation stack:

    $ make package-cf-stack

And then you can create the stack, specifying the initial `APP_VERSION` container version:

    $ APP_VERSION=1.0 make deploy-cf-stack

You can repackage and update the deployed stack from your local template file at will
by re-running the `package-cf-stack` and `deploy-cf-stack` commands, or `update-cf-stack` to do both tasks.

    $ make package-cf-stack
    $ APP_VERSION=1.0 make deploy-cf-stack
    # or
    $ APP_VERSION=1.0 make update-cf-stack

If you wish to delete the stack, you can do so,
but you must set the environment variable `DANGEROUS` to confirm that you wish to delete the stack.

    $ DANGEROUS=1 make delete-cf-stack

### Stack Configuration

Once you have a deployed stack, it needs a `config.yml` in its S3 transactions bucket.
The config file specifies notification email recipients:

    $ NOTIFICATION_EMAILS=['rob@lithic.tech','matt@lithic.tech'] make emplace-pipeline-config

### Deployment

Deployment involves building a new container image, pushing it to ECR,
and then updating the CloudFormation stack using the existing remote template:

    $ make build-deploy-image
    $ APP_VERSION=2.0 make push-image
    $ APP_VERSION=2.0 make update-stack-app-version

Or you can do it all at once with the `deploy` macro task:

    $ APP_VERSION=2.0 make deploy

These tasks do not require local CloudFormation templates and are suitable for running from a CI server.

### Exercising the Pipeline

You can trigger the full import pipeline in AWS by uploading a `transactions.csv.zip` file
to the stack's S3 transactions bucket.

If you have the standard set of sample transaction files available locally in `temp/`, you can
upload them to the shared S3 base bucket:

    $ make emplace-test-transactions # Upload sample transaction files to S3 base bucket.

Once the sample transaction files are present in the base S3 bucket, you can trigger the import
pipeline by moving a file into the stack's S3 transactions bucket as `transactions.csv.zip`:

    $ make triggerimport-1           # Copy transactions.1.csv.zip from base bucket to transactions.csv.zip
    $ make triggerimport-100k        # Same for 100k row file
    $ make triggerimport-1m          # Same for 1 million row file

The creation of `transactions.csv.zip` in the stack's S3 transactions bucket will trigger the ECS task.

### Pipeline Logs

The pipeline task emits logs to a CloudWatch LogGroup at `/ecs/[STACK]TaskDefinition`. You can tail logs locally:

    $ make logs

### Remote Access

Each stack includes a bastion host. You can SSH directly to the host:

    $ make ssh-bastion

Or you can get the IP of the host:

    $ make get-bastion-ip

and the path to the SSH key for the current AWS account and region:

    $ make get-key-path

and compose your own SSH tunneling command.

The SSH user is `ec2-user`.

You could also use output from those tasks inline in a shell command, e.g.:

    $ ssh -i $(make get-key-path) ec2-user@$(make get-bastion-ip)

If you want to create an SSH tunnel to the stack's database endpoint, there's a task for that:

    $ make ssh-pg-tunnel-bastion

This task will start an SSH tunnel from your local port 5433 to the RDS cluster port 5432 via the bastion host. You can then connect to the database by connecting to `localhost:5433`.

The database user is `dioptra`, and the database is `dioptra`. Get the password from SSM:

    $ make get-db-password

A full PostgreSQL connection string for a tunneled connection would be `postgres://dioptra:$(make get-db-password)@localhost:5433/dioptra`, so once you have a tunnel running, you could e.g.:

    $ psql postgres://dioptra:$(make get-db-password)@localhost:5433/dioptra
