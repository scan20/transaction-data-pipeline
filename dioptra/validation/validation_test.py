import unittest

from expects import *

from .. import validation, testutils


class ValidationTests(unittest.TestCase):
    def setUp(self):
        self.row = testutils.row()

    def validate(self):
        return validation.validate_row(1, self.row)

    def test_valid(self):
        result = self.validate()
        expect(result.valid()).to(be_true)
        expect(result.full_message()).to(equal(""))

    def test_errors_if_not_all_strings(self):
        with self.assertRaisesRegex(ValueError, "Every item in the CSV"):
            self.row[5] = 123
            self.validate()

    def test_row_contains_non_utf8(self):
        self.row[1] = "Hamreen Abdullah \uFFFD SAL"
        expect(self.validate().full_message()).to(
            start_with(
                "Row 1: Contains invalid characters. Re-save the file with utf-8 encoding"
            )
        )

    def test_result_formatting(self):
        self.row[1] = ""
        self.row[2] = ""
        expect(self.validate().full_message()).to(
            equal(
                "Row 1: Country code (column B) cannot be empty, Grant code (column C) cannot be empty"
            )
        )

    def test_row_length_too_short(self):
        self.row = testutils.row(0)
        self.row.pop()
        expect(self.validate().full_message()).to(
            equal("Row 1: 12 to 17 columns required (got 11)")
        )

    def test_row_length_too_long(self):
        self.row = testutils.row(5)
        self.row.append("")
        expect(self.validate().full_message()).to(
            equal("Row 1: 12 to 17 columns required (got 18)")
        )

    def test_date_required(self):
        self.row[0] = ""
        expect(self.validate().full_message()).to(
            equal("Row 1: Transaction date (column A) cannot be empty")
        )

    def test_date_format(self):
        self.row[0] = "123"
        expect(self.validate().full_message()).to(
            equal(
                "Row 1: Transaction date (column A) must be in the format YYYY-MM-DD (got 123)"
            )
        )

    def test_country_code_required(self):
        self.row[1] = ""
        expect(self.validate().full_message()).to(
            equal("Row 1: Country code (column B) cannot be empty")
        )

    def test_country_code_length(self):
        self.row[1] = long_string
        expect(self.validate().full_message()).to(
            equal("Row 1: Country code (column B) is longer than 255 characters")
        )

    def test_grant_code_required(self):
        self.row[2] = ""
        expect(self.validate().full_message()).to(
            equal("Row 1: Grant code (column C) cannot be empty")
        )

    def test_grant_code_length(self):
        self.row[2] = long_string
        expect(self.validate().full_message()).to(
            equal("Row 1: Grant code (column C) is longer than 255 characters")
        )

    def test_budget_line_code_required(self):
        self.row[3] = ""
        expect(self.validate().full_message()).to(
            equal("Row 1: Budget line code (column D) cannot be empty")
        )

    def test_budget_line_code_length(self):
        self.row[3] = long_string
        expect(self.validate().full_message()).to(
            equal("Row 1: Budget line code (column D) is longer than 255 characters")
        )

    def test_account_code_required(self):
        self.row[4] = ""
        expect(self.validate().full_message()).to(
            equal("Row 1: Account code (column E) cannot be empty")
        )

    def test_account_code_length(self):
        self.row[4] = long_string
        expect(self.validate().full_message()).to(
            equal("Row 1: Account code (column E) is longer than 255 characters")
        )

    def test_site_code_length(self):
        self.row[5] = long_string
        expect(self.validate().full_message()).to(
            equal("Row 1: Site code (column F) is longer than 255 characters")
        )

    def test_sector_code_length(self):
        self.row[6] = long_string
        expect(self.validate().full_message()).to(
            equal("Row 1: Sector code (column C) is longer than 255 characters")
        )

    def test_transaction_code_length(self):
        self.row[7] = long_string
        expect(self.validate().full_message()).to(
            equal("Row 1: Transaction code (column H) is longer than 255 characters")
        )

    # transaction description is always valid

    def test_currency_code_enum(self):
        self.row[9] = "US"
        expect(self.validate().full_message()).to(
            equal("Row 1: Currency code (column J) is an invalid currency code (got US)")
        )

    def test_budget_line_description_length(self):
        self.row[10] = long_string
        expect(self.validate().full_message()).to(
            equal("Row 1: Budget line description (column K) is longer than 255 characters")
        )

    def test_amount_float(self):
        self.row[11] = "e102"
        expect(self.validate().full_message()).to(
            equal("Row 1: Amount (column L) is not a number (got e102)")
        )

    def test_dummy_1_length(self):
        self.row = testutils.row(1)
        self.row[12] = long_string
        expect(self.validate().full_message()).to(
            equal("Row 1: Dummy field 1 (column M) is longer than 255 characters")
        )

    def test_dummy_2_length(self):
        self.row = testutils.row(2)
        self.row[13] = long_string
        expect(self.validate().full_message()).to(
            equal("Row 1: Dummy field 2 (column N) is longer than 255 characters")
        )

    def test_dummy_3_length(self):
        self.row = testutils.row(3)
        self.row[14] = long_string
        expect(self.validate().full_message()).to(
            equal("Row 1: Dummy field 3 (column O) is longer than 255 characters")
        )

    def test_dummy_4_length(self):
        self.row = testutils.row(4)
        self.row[15] = long_string
        expect(self.validate().full_message()).to(
            equal("Row 1: Dummy field 4 (column P) is longer than 255 characters")
        )

    def test_dummy_5_length(self):
        self.row = testutils.row(5)
        self.row[16] = long_string
        expect(self.validate().full_message()).to(
            equal("Row 1: Dummy field 5 (column Q) is longer than 255 characters")
        )


long_string = "a" * 256
