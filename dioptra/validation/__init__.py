import re
from typing import List, Optional


class ValidationResult(object):
    def __init__(self, row_index_in_csv):
        self.row_index_in_csv = row_index_in_csv
        self._errors = []

    def add_error(self, message: str):
        self._errors.append(message)
        return True

    def check(self, value, prefix, *checkers):
        for checker in checkers:
            if checker(value, prefix):
                return

    def check_optional(self, row, index, prefix, *checkers):
        try:
            value = row[index]
        except IndexError:
            return
        return self.check(value, prefix, *checkers)

    def date(self, value, prefix):
        if not _date.match(value):
            return self.add_error(f"{prefix} must be in the format YYYY-MM-DD (got {value})")

    def require(self, value, prefix):
        if not value:
            return self.add_error(prefix + " cannot be empty")

    def shortlength(self, value, prefix):
        if len(value) > 255:
            return self.add_error(prefix + " is longer than 255 characters")

    def currency(self, value, prefix):
        if value not in _currencies:
            return self.add_error(f"{prefix} is an invalid currency code (got {value})")

    def float(self, value, prefix):
        try:
            float(value)
            return
        except ValueError:
            return self.add_error(f"{prefix} is not a number (got {value})")

    def valid(self):
        return len(self._errors) == 0

    def full_message(self):
        if self.valid():
            return ""
        return f'Row {self.row_index_in_csv}: {", ".join(self._errors)}'


def validate_row(index: int, row: List) -> Optional[ValidationResult]:
    v = ValidationResult(index)
    if not 12 <= len(row) <= 17:
        v.add_error(f"12 to 17 columns required (got {len(row)})")
        return v
    for cell in row:
        if not isinstance(cell, str):
            raise ValueError(
                "Every item in the CSV row must be a string, this is a programming error"
            )
        # We use 'errors=replace' for decoding text. See importer code for details.
        if "\uFFFD" in cell:
            v.add_error(
                "Contains invalid characters. Re-save the file with utf-8 encoding, "
                "or remove the funny-looking characters"
            )
            return v

    v.check(row[0], "Transaction date (column A)", v.require, v.date)
    v.check(row[1], "Country code (column B)", v.require, v.shortlength)
    v.check(row[2], "Grant code (column C)", v.require, v.shortlength)
    v.check(row[3], "Budget line code (column D)", v.require, v.shortlength)
    v.check(row[4], "Account code (column E)", v.require, v.shortlength)
    v.check(row[5], "Site code (column F)", v.shortlength)
    v.check(row[6], "Sector code (column C)", v.shortlength)
    v.check(row[7], "Transaction code (column H)", v.shortlength)
    # v.check(row[8], 'Transaction description (column I)', v.shortlength)
    v.check(row[9], "Currency code (column J)", v.require, v.currency)
    v.check(row[10], "Budget line description (column K)", v.require, v.shortlength)
    v.check(row[11], "Amount (column L)", v.require, v.float)
    v.check_optional(row, 12, "Dummy field 1 (column M)", v.shortlength)
    v.check_optional(row, 13, "Dummy field 2 (column N)", v.shortlength)
    v.check_optional(row, 14, "Dummy field 3 (column O)", v.shortlength)
    v.check_optional(row, 15, "Dummy field 4 (column P)", v.shortlength)
    v.check_optional(row, 16, "Dummy field 5 (column Q)", v.shortlength)
    return v


_date = re.compile(r"^\d\d\d\d-\d\d-\d\d$")

_currencies = frozenset(
    [
        "AED",
        "AFN",
        "ALL",
        "AMD",
        "ANG",
        "AOA",
        "ARS",
        "AUD",
        "AWG",
        "AZN",
        "BAM",
        "BBD",
        "BDT",
        "BGN",
        "BHD",
        "BIF",
        "BMD",
        "BND",
        "BOB",
        "BOV",
        "BRL",
        "BSD",
        "BTN",
        "BWP",
        "BYN",
        "BZD",
        "CAD",
        "CDF",
        "CHE",
        "CHF",
        "CHW",
        "CLF",
        "CLP",
        "CNY",
        "COP",
        "COU",
        "CRC",
        "CUC",
        "CUP",
        "CVE",
        "CZK",
        "DJF",
        "DKK",
        "DOP",
        "DZD",
        "EGP",
        "ERN",
        "ETB",
        "EUR",
        "FJD",
        "FKP",
        "GBP",
        "GEL",
        "GHS",
        "GIP",
        "GMD",
        "GNF",
        "GTQ",
        "GYD",
        "HKD",
        "HNL",
        "HRK",
        "HTG",
        "HUF",
        "IDR",
        "ILS",
        "INR",
        "IQD",
        "IRR",
        "ISK",
        "JMD",
        "JOD",
        "JPY",
        "KES",
        "KGS",
        "KHR",
        "KMF",
        "KPW",
        "KRW",
        "KWD",
        "KYD",
        "KZT",
        "LAK",
        "LBP",
        "LKR",
        "LRD",
        "LSL",
        "LYD",
        "MAD",
        "MDL",
        "MGA",
        "MKD",
        "MMK",
        "MNT",
        "MOP",
        "MRU",
        "MUR",
        "MVR",
        "MWK",
        "MXN",
        "MXV",
        "MYR",
        "MZN",
        "NAD",
        "NGN",
        "NIO",
        "NOK",
        "NPR",
        "NZD",
        "OMR",
        "PAB",
        "PEN",
        "PGK",
        "PHP",
        "PKR",
        "PLN",
        "PYG",
        "QAR",
        "RON",
        "RSD",
        "RUB",
        "RWF",
        "SAR",
        "SBD",
        "SCR",
        "SDG",
        "SEK",
        "SGD",
        "SHP",
        "SLL",
        "SOS",
        "SRD",
        "SSP",
        "STN",
        "SVC",
        "SYP",
        "SZL",
        "THB",
        "TJS",
        "TMT",
        "TND",
        "TOP",
        "TRY",
        "TTD",
        "TWD",
        "TZS",
        "UAH",
        "UGX",
        "USD",
        "USN",
        "UYI",
        "UYU",
        "UYW",
        "UZS",
        "VES",
        "VND",
        "VUV",
        "WST",
        "XAF",
        "XAG",
        "XAU",
        "XBA",
        "XBB",
        "XBC",
        "XBD",
        "XCD",
        "XDR",
        "XOF",
        "XPD",
        "XPF",
        "XPT",
        "XSU",
        "XUA",
        "YER",
        "ZAR",
        "ZMW",
        "ZWL",
    ]
)
