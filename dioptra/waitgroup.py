import threading


class WaitGroup(object):
    """WaitGroup is a synchronization primitive that supports a 'fan out and wait' pattern.
    The main thread creates the wait group, and calls `incr()` when it spawns some work.

    After all the work is spawned, the main thread calls wait(), which blocks.

    As workers (on other threads) finish their tasks, they call `done()`.
    When the number of done tasks matches the number of added tasks,
    any thread blocking via `wait()` is released.

    Note: Make sure you call `incr()` on the main thread/before you start the background work!
    Workers/background threads should call `done()` but not `incr()` or
    you'll end up with a race condition.
    """

    def __init__(self):
        self._num_added = 0
        self._num_finished = 0
        self._event = threading.Event()
        # If we never increment, make sure wait does not hang
        self._event.set()

    def incr(self):
        self._num_added += 1
        self._event.clear()

    def done(self):
        self._num_finished += 1
        if self._num_added == self._num_finished:
            self._event.set()

    def wait(self):
        self._event.wait()
