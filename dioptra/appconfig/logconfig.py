"""
Configure logging appropriately for your app.

Set APP_LOG_LEVEL to control the log level of our code.

You can set LOG_LEVEL to control logging.basicConfig level
which can turn on debug logging for other libraies (like Boto).

Set LOG_FORMAT to override the log format.
Default is json if stdout is a tty,
console/keyvalue if not.
Set to 'json' for JSON, 'keyvalue' for keyvalue.
Default if set and unknown is 'json'.
"""

import logging
import sys

import structlog

from ._env import env


app_log_level_name = env("APP_LOG_LEVEL", "DEBUG").upper()
app_log_level = logging._nameToLevel[app_log_level_name]

global_log_level_name = env("LOG_LEVEL", "").upper()
global_log_level = logging._nameToLevel.get(global_log_level_name)

log_format = env("LOG_FORMAT", "").lower()


def filter_app_level(_logger, name, event_dict):
    lvl = logging._nameToLevel[name.upper()]
    if lvl >= app_log_level:
        return event_dict
    raise structlog.DropEvent


def configure():
    if not log_format and sys.stdout.isatty():
        renderer = structlog.dev.ConsoleRenderer(colors=structlog.dev.colorama is not None)
    elif not log_format:
        renderer = structlog.processors.JSONRenderer()
    elif log_format == "keyvalue":
        renderer = structlog.processors.KeyValueRenderer()
    else:
        renderer = structlog.processors.JSONRenderer()

    structlog.configure_once(
        processors=[
            filter_app_level,
            structlog.processors.TimeStamper(fmt="iso"),
            structlog.stdlib.add_log_level,
            structlog.stdlib.PositionalArgumentsFormatter(),
            structlog.processors.StackInfoRenderer(),
            structlog.processors.format_exc_info,
            structlog.processors.UnicodeDecoder(),
            structlog.processors.ExceptionPrettyPrinter(),
            renderer,
        ],
        context_class=dict,
        cache_logger_on_first_use=True,
    )
    if global_log_level:
        logging.basicConfig(level=global_log_level)
