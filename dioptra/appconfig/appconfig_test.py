import unittest
from .. import testutils, appconfig


class AppconfigTests(unittest.TestCase):
    def test_log_exception_works_with_no_fields(self):
        with testutils.assert_log_event("blah", log_level="critical"):
            with appconfig.log_exception("blah", reraise=False):
                raise Exception("Hello")

    def test_log_exception_works_with_fields(self):
        with testutils.assert_log_event("blah", log_level="critical", myfield=4):
            with appconfig.log_exception("blah", reraise=False, fields={"myfield": 4}):
                raise Exception("Hello")
