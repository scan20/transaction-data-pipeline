from environs import Env

env = Env()
# noinspection PyUnresolvedReferences
env.read_env()
