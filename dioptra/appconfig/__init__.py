from contextlib import contextmanager
import csv

import structlog

from . import logconfig
from ._env import env

build_sha = env("BUILD_SHA", "unknown")
build_time = env("BUILD_TIME", "unknown")
csv_filename = env("CSV_FILENAME", "transactions.csv")
appname = env("APPLICATION_NAME", "dioptra-pipeline")
delim = ","
default_s3_bucket = "dioptra-transactions"
default_s3_archive_key = "transactions.csv.zip"
default_s3_config_key = "config.yml"
default_from_email = "no-reply@scantool.org"
sentry_dsn = env("SENTRY_DSN", "")


class ConfigurationError(Exception):
    pass


def _required(key):
    def validate(value):
        if not value:
            raise ConfigurationError(f"{key} is required")
        return value

    return validate


# noinspection PyUnresolvedReferences
# Config enumerates all config values for the app.
# If you add a value here you need configured in the running app,
# it must be added to fargate.yaml and in the Makefile.
class Config(object):
    postgres_url = env("DATABASE_URL", validate=_required("DATABASE_URL"))
    max_validation_errors = env.int("MAX_VALIDATION_ERRORS", 50)
    chunk_size = env.int("CHUNK_SIZE", 100000)
    max_database_connections = env.int("MAX_DATABASE_CONNECTIONS", 30)
    perfmon_interval = env.int("PERFMON_INTERVAL", 5)
    thread_pool_size = env.int("THREAD_POOL_SIZE", 20)
    from_email = env("FROM_EMAIL", default_from_email)
    s3_endpoint = env("S3_ENDPOINT", "")
    s3_bucket = env("S3_BUCKET", default_s3_bucket)
    s3_archive_key = env("S3_ARCHIVE_KEY", default_s3_archive_key)
    s3_config_key = env("S3_CONFIG_KEY", default_s3_config_key)
    ses_endpoint = env("SES_ENDPOINT", "")
    ses_region = env("SES_REGION", "")
    aws_region = env("AWS_REGION", "us-east-1")
    append_mode = env.bool("APPEND_MODE", False)


def get_logger():
    """Return a structlog logger with immutable app-level config keys set.
    Should assign a module-level logger because we should not create a logger before we've run configuration.
    """
    return structlog.get_logger().bind(app=appname, build_sha=build_sha, build_time=build_time)


@contextmanager
def log_exception(event, reraise=True, logger=None, fields=None):
    logger = logger or get_logger()
    fields = fields or {}
    try:
        yield
        return True
    except Exception as ex:
        logger.bind(error=ex, **fields).critical(event)
        if reraise:
            raise
        return False


def csv_reader(f):
    return csv.reader(f, delimiter=delim)


def csv_writer(f):
    return csv.writer(f, delimiter=delim, quotechar='"', quoting=csv.QUOTE_MINIMAL)
