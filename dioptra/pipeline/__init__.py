"""
Glue code that ties the CSV importing behavior,
pipeline configuration, and notifications all together.
"""
import io
from typing import List

import boto3
import yaml
from pydantic import BaseModel, EmailStr

from .. import appconfig, context, importer, notifier


class PipelineConfig(BaseModel):
    notification_emails: List[EmailStr] = []


def run(ctx: context.Context):
    ctx.stage_timing.set_pipeline_started()
    pipeconfig = download_config(ctx)
    transactions_stream = start_transaction_download(ctx)
    imp = importer.Importer(transactions_stream, ctx)
    imp_result = imp.run()
    log_ctx = dict(
        rows_read=imp_result.rows_read,
        rows_processed=imp_result.rows_processed,
        rows_written=imp_result.rows_written,
        old_row_count=imp_result.old_row_count,
        new_row_count=imp_result.new_row_count,
        total_duration=ctx.stage_timing.total_duration.total_seconds(),
        waiting_for_start_duration=ctx.stage_timing.waiting_for_start.total_seconds(),
        import_duration=ctx.stage_timing.total_importer.total_seconds(),
        validation_errors=len(imp_result.validation_errors),
    )
    log_method = "info"
    if imp_result.exception:
        log_ctx["error"] = imp_result.exception
        log_method = "error"
    getattr(ctx.logger.bind(**log_ctx), log_method)("import_pipeline_finished")
    if len(pipeconfig.notification_emails) > 0:
        notifier.send_email(ctx, pipeconfig.notification_emails, imp_result)
    else:
        ctx.logger.warn("no_notification_emails")
    return imp_result


def download_config(ctx: context.Context) -> PipelineConfig:
    with ctx.perf_agg.time("download_config"):
        with appconfig.log_exception("download_config_yaml_error"):
            pipeline_resource = s3_resource(ctx.config)
            try:
                pipeline_bytes = (
                    pipeline_resource.Object(
                        bucket_name=ctx.config.s3_bucket, key=ctx.config.s3_config_key
                    )
                    .get()["Body"]
                    .read()
                )
            except pipeline_resource.meta.client.exceptions.NoSuchKey:
                return PipelineConfig()
        with appconfig.log_exception("parse_config_yaml_error"):
            pipeconfig_data = yaml.safe_load(pipeline_bytes)
        with appconfig.log_exception("load_config_yaml_error"):
            pipeconfig = PipelineConfig.parse_obj(pipeconfig_data)
        return pipeconfig


def start_transaction_download(ctx: context.Context):
    with ctx.perf_agg.time("start_download_transactions"):
        with appconfig.log_exception("download_transactions_zip_error"):
            transactions_obj = s3_resource(ctx.config).Object(
                bucket_name=ctx.config.s3_bucket, key=ctx.config.s3_archive_key
            )
            # Check the content length by making a HEAD call, to check the file is there and reachable.
            # Otherwise, the error will happen later, during import, which is more annoying to handle.
            _ = transactions_obj.content_length
            ctx.stage_timing.set_transactions_file_last_modified(
                transactions_obj.last_modified
            )
        b = io.BytesIO()
        b.write(transactions_obj.get()["Body"].read())
        b.seek(0)
        return b


def s3_resource(config: appconfig.Config):
    return boto3.resource(
        "s3", region_name=config.aws_region, endpoint_url=config.s3_endpoint or None
    )
