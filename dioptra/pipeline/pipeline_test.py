import random
import unittest

import boto3
import botocore.exceptions
import moto
import yaml.parser
from expects import *

from .. import testutils, notifier, pipeline, context


@moto.mock_s3
@moto.mock_ses
class PipelineTests(unittest.TestCase):
    def setUp(self):
        moto.s3.s3_backend.reset()
        moto.ses.ses_backend.reset()

        self.ctx = context.tester()
        self.ctx.config.s3_bucket = "buckit"
        self.ctx.config.s3_config_key = "s3con"
        self.ctx.config.s3_archive_key = "s3tr"

        notifier.ses_client(self.ctx.config).verify_email_identity(
            EmailAddress=self.ctx.config.from_email
        )
        self.s3conn = boto3.client("s3", region_name=self.ctx.config.aws_region)
        self.s3conn.create_bucket(Bucket=self.ctx.config.s3_bucket)

        self.valid_config = 'notification_emails: ["hi@ombuweb.com"]'

    def csv_zip_with_rows(self, n):
        return testutils.zip_transactions(testutils.to_csv(testutils.rows(n)))

    def putobj(self, key, body):
        self.s3conn.put_object(Bucket=self.ctx.config.s3_bucket, Key=key, Body=body)

    def test_fatal_error_if_config_malformed(self):
        self.putobj(self.ctx.config.s3_config_key, "{]")
        with testutils.assert_log_event("parse_config_yaml_error"):
            with self.assertRaises(yaml.parser.ParserError):
                pipeline.run(self.ctx)

    def test_fatal_error_if_s3_csv_zip_missing(self):
        self.putobj(self.ctx.config.s3_config_key, self.valid_config)
        with testutils.assert_log_event("download_transactions_zip_error"):
            with self.assertRaises(botocore.exceptions.ClientError):
                pipeline.run(self.ctx)

    def test_import_fails_if_s3_transactions_file_has_issues(self):
        self.putobj(self.ctx.config.s3_config_key, self.valid_config)
        self.putobj(
            self.ctx.config.s3_archive_key,
            testutils.zip_transactions("", "wrong-filename.csv"),
        )
        result = pipeline.run(self.ctx)
        expect(result.exception).to(be_a(KeyError))
        expect(result.exception.args[0]).to(
            equal("There is no item named 'transactions.csv' in the archive")
        )

    def test_runs_importer(self):
        row_count = random.randint(3, 25)
        self.putobj(self.ctx.config.s3_config_key, self.valid_config)
        self.putobj(self.ctx.config.s3_archive_key, self.csv_zip_with_rows(row_count))

        result = pipeline.run(self.ctx)

        send_quota = notifier.ses_client(self.ctx.config).get_send_quota()
        expect(send_quota).to(have_key("SentLast24Hours", be_above_or_equal(1)))

        expect(result).to(
            have_properties(
                rows_written=row_count,
                new_row_count=row_count,
                validation_errors=[],
                exception=None,
            )
        )

    def test_runs_import_and_skips_email_if_config_file_missing(self):
        self.putobj(self.ctx.config.s3_archive_key, self.csv_zip_with_rows(1))
        result = pipeline.run(self.ctx)
        expect(result).to(
            have_properties(
                rows_written=1,
                new_row_count=1,
                validation_errors=[],
                exception=None,
            )
        )
        expect(notifier.ses_client(self.ctx.config).get_send_quota()).to(
            have_key("SentLast24Hours", 0)
        )

    def test_runs_import_and_skips_email_if_config_has_no_emails(self):
        self.putobj(self.ctx.config.s3_config_key, "{}")
        self.putobj(self.ctx.config.s3_archive_key, self.csv_zip_with_rows(1))
        result = pipeline.run(self.ctx)
        expect(result).to(have_property("exception", None))
        expect(notifier.ses_client(self.ctx.config).get_send_quota()).to(
            have_key("SentLast24Hours", 0)
        )
