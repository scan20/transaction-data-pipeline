from dioptra import context, pipeline, appconfig

if __name__ == "__main__":
    appconfig.logconfig.configure()
    ctx = context.from_config(appconfig.Config())
    pipeline.run(ctx)
    ctx.logger.bind(**ctx.perf_agg.log_fields()).info("perfagg")
