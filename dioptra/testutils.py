import os
import random
import zipfile
from contextlib import contextmanager
from inspect import currentframe
from io import StringIO, BytesIO

import structlog
from expects import *

from . import appconfig


def testdata(tail):
    """Return the absolute path to a file in the testdata folder.

    WARNING: Only use testdata files in rare cases (like to test certain edge cases
    in the importer, like encoding errors that are hard to fixture).
    It's difficult to maintain external data files- instead, fixture data in-memory
    as mosts tests do.
    """
    return os.path.join(os.path.dirname(__file__), "..", "testdata", tail)


def rows(n, dummies=None):
    """Return a list of _n_ rows, each row with all required fields and _dummies_ dummy values.
    We need this and not just `[testutils.row() for _ in range(10)]`
    because the number of dummies in each row must be the same
    (a CSV is not valid unless all rows have the same number of cells).
    """
    d = random.randint(0, 5) if dummies is None else dummies
    return [row(d) for _ in range(n)]


def row(dummies=None):
    d = random.randint(0, 5) if dummies is None else dummies
    return [
        "1993-10-02",
        "LB",
        "9116",
        "317363",
        "5501",
        "",
        "",
        "",
        "",
        "SBD",
        "Pharmacist community",
        "-19243.9",
    ] + dummy_rows[:d]


dummy_rows = [
    "Record general other when.",
    "Alone court figure role level.",
    "Off return present defense thank.",
    "",
    "Sell difference forget decade food off.",
]


def invalid_row():
    r = row()
    r[0] = ""
    return r


def to_csv(rows_):
    io = StringIO()
    writer = appconfig.csv_writer(io)
    for r in rows_:
        writer.writerow(r)
    io.flush()
    io.seek(0)
    return io.read()


def zip_transactions(csv_str, filename="transactions.csv"):
    io = BytesIO()
    with zipfile.ZipFile(io, "w", zipfile.ZIP_STORED) as myzip:
        myzip.writestr(filename, csv_str)
    io.flush()
    io.seek(0)
    return io


def get_linenumber():
    cf = currentframe()
    return cf.f_back.f_lineno


class LogCapture:
    def __init__(self):
        self.entries = []

    def __call__(self, _, method_name, event_dict):
        event_dict["log_level"] = method_name
        self.entries.append(event_dict)
        raise structlog.DropEvent


@contextmanager
def capture_logs():
    cap = LogCapture()
    old_processors = structlog.get_config()["processors"]
    try:
        structlog.configure(processors=[cap])
        yield cap.entries
    finally:
        structlog.configure(processors=old_processors)


@contextmanager
def assert_log_event(event, **kwargs):
    with capture_logs() as logs:
        yield
    expect(logs).to(contain(have_keys(dict(event=event, **kwargs))))
