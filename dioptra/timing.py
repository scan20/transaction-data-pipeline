import datetime
import threading
from contextlib import contextmanager
import time
from typing import Callable
from warnings import warn

import resource


def is_naive_datetime(dt):
    """Return true if the datetime is timezone 'naive'.
    See https://docs.python.org/3/library/datetime.html#determining-if-an-object-is-aware-or-naive.
    However, this is a lie, there are other ways to have naive datetimes.
    Rather than check them all, we can check if they can be manipulated with an aware datetime,
    and if they error, they cannot.
    """
    try:
        dt - _tzaware
        return False
    except TypeError as ex:
        # print('err', repr(dt), dt.tzinfo, repr(ex.args), dt.strftime('%Y-%m-%d %H:%M:%S %Z (%z)'))
        return "can't subtract offset-naive and offset-aware datetimes" in ex.args


_tzaware = datetime.datetime.now().astimezone()
if is_naive_datetime(_tzaware):
    raise SystemExit(
        f'cannot make timezone aware datetime ("{_tzaware}"), this module cannot run.'
    )


@contextmanager
def log_timing(logger, msg):
    start = time.perf_counter()
    logger.debug(f"{msg}_start")
    try:
        yield
    except BaseException:
        logger.debug(f"{msg}_error")
        raise
    finally:
        logger = logger.bind(**{f"{msg}_elapsed": time.perf_counter() - start})
        logger.info(f"{msg}_finish")


class Perfmon(threading.Thread):
    """Log out performance information on an interval."""

    def __init__(self, logger, get_fields: Callable[[], dict], interval=5):
        super().__init__()
        self._logger = logger
        self._get_fields = get_fields
        self._interval = interval
        self._stopped_flag = threading.Event()
        self._started_tick = 0
        self._last_tick = 0

    def run(self):
        self._started_tick = time.perf_counter()
        self._last_tick = self._started_tick
        while not self._stopped_flag.wait(self._interval):
            self._log()

    def _log(self):
        now_tick = time.perf_counter()
        self._logger.bind(
            elapsed_period=now_tick - self._last_tick,
            elapsed_total=now_tick - self._started_tick,
            maxrss=resource.getrusage(resource.RUSAGE_SELF).ru_maxrss,
            **self._get_fields(),
        ).debug("perfmon")
        self._last_tick = now_tick

    def cancel(self):
        self._stopped_flag.set()
        self._log()


_active_perfagg = threading.local()
_active_perfagg.v = ""


class PerfAgg(object):
    """Use context managers to record elapsed time for different labels."""

    def __init__(self):
        self._totals = {}

    @contextmanager
    def time(self, key):
        if hasattr(_active_perfagg, "v") and _active_perfagg.v:
            warn(f"PerfAgg {key} nested under {_active_perfagg.v}, remove the call please")
            yield
        else:
            self._totals.setdefault(key, 0)
            _active_perfagg.v = key

            start = time.perf_counter()
            try:
                yield
            finally:
                self._totals[key] += time.perf_counter() - start
                _active_perfagg.v = ""

    def totals(self):
        return dict(self._totals)

    def log_fields(self):
        total = sum(self.totals().values())
        d = {"perfagg_total": total}
        for (k, v) in sorted(self.totals().items()):
            percent = v / total
            d[f"perfagg_{k}"] = v
            d[f"perfagg_{k}_p"] = int(percent * 100)
        return d


class Stages(object):
    """Record when different stages of the pipeline start/end.
    Using this is a little nuanced, so we can support stage timing even when running incomplete stages.

    - When running an entire import from a file in S3,
      we let the various parts of the import mutate the appropriate stage.
      For example, after we download the file from s3 in the pipeline part, we set the timestamp.
    - When running just a part of the pipeline, you probably want to run `reset`,
      to initialize the timestamps to now. Otherwise, you will get durations for stages
      that start at the unix epoch.
    """

    _transactions_file_last_modified = datetime.datetime.min
    _pipeline_started = datetime.datetime.min
    _import_started = datetime.datetime.min
    _import_finished = datetime.datetime.min

    def reset(self, t=None):
        t = t or self._dt(None)  # Ensure all are set to the same value, call now() just once.
        self.set_transactions_file_last_modified(t)
        self.set_pipeline_started(t)
        self.set_import_started(t)
        self.set_import_finished(t)

    def _dt(self, t):
        if isinstance(t, int):
            return datetime.datetime.fromtimestamp(t)
        # S3 gives us naive datetimes.
        # See https://stackoverflow.com/questions/7065164/-/7065242
        # This sort of thing is hellishly confusing in python because it doesn't have a unified
        # datetime and timezone type, it's mishmashed among many stdlib libraries.
        if isinstance(t, datetime.datetime):
            if is_naive_datetime(t):
                t = t.replace(tzinfo=_tzaware.tzinfo)
            return t
        if t is not None:
            return t
        return datetime.datetime.now().astimezone()

    def set_transactions_file_last_modified(self, t=None):
        self._transactions_file_last_modified = self._dt(t)

    def set_pipeline_started(self, t=None):
        self._pipeline_started = self._dt(t)

    def set_import_started(self, t=None):
        self._import_started = self._dt(t)

    def set_import_finished(self, t=None):
        self._import_finished = self._dt(t)

    @property
    def total_duration(self):
        return self._import_finished - self._transactions_file_last_modified

    @property
    def waiting_for_start(self):
        return self._pipeline_started - self._transactions_file_last_modified

    @property
    def importer_prep(self):
        return self._import_started - self._pipeline_started

    @property
    def importer(self):
        return self._import_finished - self._import_started

    @property
    def total_importer(self):
        return self._import_finished - self._pipeline_started
