import datetime
from collections import namedtuple
from io import StringIO
from typing import Callable, List

import boto3

from dioptra import appconfig, importer, context

Rendered = namedtuple("Rendered", ["subject", "body"])
Template = Callable[[context.Context, importer.ImportResult], Rendered]


def send_email(
    ctx: context.Context,
    notification_emails: List[str],
    result: importer.ImportResult,
):
    if result.success:
        template = success_template
    elif result.missing_transactions_csv:
        template = missing_csv_template
    elif result.exception:
        template = exception_template
    else:
        template = validation_errors_template

    ctx = ctx.bind(
        email_from=ctx.config.from_email,
        email_template_function=template.__name__,
        email_recipients=list(notification_emails),
    )
    rendered = template(ctx, result)
    ctx.logger.info("sending_email_ses")
    send_ses(ctx, notification_emails, rendered.subject, rendered.body)


def send_ses(ctx: context.Context, recipients: List[str], subject: str, body: str):
    """Sends an email using Amazon SES."""
    ctx.logger.bind(email_subject=subject, email_body=body).debug("ses_send_contents")
    ses_client(ctx.config).send_email(
        Source=ctx.config.from_email,
        Destination={"ToAddresses": list(recipients)},
        Message={
            "Subject": {"Data": subject, "Charset": "UTF-8"},
            "Body": {
                "Text": {"Data": body, "Charset": "UTF-8"},
            },
        },
        ReplyToAddresses=[ctx.config.from_email],
    )


def ses_client(config: appconfig.Config):
    return boto3.client(
        "ses",
        region_name=config.ses_region or config.aws_region,
        endpoint_url=config.ses_endpoint or None,
    )


def success_template(ctx: context.Context, ir: importer.ImportResult):
    subject = "Dioptra transaction import: Success"

    if ctx.config.append_mode:
        added_rows = ir.new_row_count - ir.old_row_count
        load_line = f"Dioptra successfully loaded {added_rows:,} additional transactions to the data store."
    else:
        load_line = (
            f"Dioptra successfully loaded {ir.new_row_count:,} transactions to the data store."
        )

    body = f"""{load_line}

There were {ir.old_row_count:,} transactions in the data store at the time the job started.
The transaction data store now has {ir.new_row_count:,} transactions.
The job took {format_duration(ctx.stage_timing.total_duration)} to run.
{_footer()}
"""
    return Rendered(subject, body)


def validation_errors_template(ctx: context.Context, ir: importer.ImportResult):
    subject = "Dioptra transaction import: Failure"
    all_errors = sorted(ir.validation_errors, key=lambda ve: ve.row_index_in_csv)
    errors_to_print = all_errors[: ctx.config.max_validation_errors]
    body = StringIO()
    if len(all_errors) == len(errors_to_print):
        body.write(
            f"The following {len(all_errors)} errors were found when trying to load the transaction data:\n\n"
        )
    else:
        body.write(
            f"At least {len(all_errors)} errors were found trying to load the transaction data.\n"
        )
        body.write(f"Here are the first {len(errors_to_print)}:\n\n")
    for vr in errors_to_print:
        body.write(f"- {vr.full_message()}\n")
    body.write("\n")
    body.write(
        f"""There were {ir.old_row_count:,} transactions in the data store at the time the job started.
The transactions were unchanged because the job failed.
The job took {format_duration(ctx.stage_timing.total_duration)} to run.
{_footer()}
"""
    )
    return Rendered(subject, body.getvalue())


def missing_csv_template(_ctx: context.Context, ir: importer.ImportResult):
    subject = "Dioptra transaction import: Archive missing transactions.csv"
    if len(ir.zip_infos) > 0:
        file_info_txt = "\n".join(
            [f"- {zi.filename} ({zi.file_size} bytes)" for zi in ir.zip_infos]
        )
        body = f"""The uploaded transactions.csv.zip file uploaded was missing a transactions.csv file.
The contents of the archive are:

{file_info_txt}
{_footer()}
"""
    else:
        body = f"""The uploaded transactions.csv.zip file uploaded was missing a transactions.csv file.
The archive was empty.
{_footer()}
"""
    return Rendered(subject, body)


def exception_template(ctx: context.Context, ir: importer.ImportResult):
    subject = "Dioptra transaction import: Unexpected Error"
    body = f"""An unexpected error occurred when trying to load the transaction data.

There were {ir.old_row_count:,} transactions in the data store at the time the job started.
The transactions were unchanged because the job failed.
The job took {format_duration(ctx.stage_timing.total_duration)} to run.

* Error ID {ir.reported_exception_id}
* Trace ID {ctx.trace_id}
{_footer()}
"""
    return Rendered(subject, body)


def format_duration(d: datetime.timedelta):
    seconds = round(d.total_seconds())
    minutes, seconds = divmod(seconds, 60)
    minpart = "minute" if minutes == 1 else "minutes"
    secpart = "second" if seconds == 1 else "seconds"
    if minutes == 0:
        return f"{seconds:0.0f} {secpart}"
    if seconds == 0:
        return f"{minutes:0.0f} {minpart}"
    return f"{minutes:0.0f} {minpart} {seconds:0.0f} {secpart}"


def _footer():
    return f"\n\nPipeline build {appconfig.build_sha} at {appconfig.build_time}"
