import datetime
import random
import unittest
import zipfile
from unittest import mock

from expects import *

from .. import importer, validation, context
from . import (
    exception_template,
    format_duration,
    missing_csv_template,
    send_email,
    send_ses,
    success_template,
    validation_errors_template,
)

missing_csv_error = KeyError("There is no item named 'transactions.csv' in the archive")


class NotifierTests(unittest.TestCase):
    def setUp(self):
        self.ctx = context.tester()
        self.ir = importer.ImportResult(10)

    @mock.patch("dioptra.notifier.send_ses")
    def test_calls_ses_with_configured_values(self, send):
        send_email(self.ctx, ["hello@ombuweb.com"], self.ir)
        send.assert_called_once()
        expect(send.call_args[0][0]).to(be_a(context.Context))
        expect(send.call_args[0][1]).to(equal(["hello@ombuweb.com"]))

    @mock.patch("dioptra.notifier.send_ses")
    def test_uses_success_template_on_success(self, send):
        send_email(self.ctx, [], self.ir)
        send.assert_called_once()
        expect(send.call_args[0][2]).to(equal("Dioptra transaction import: Success"))

    @mock.patch("dioptra.notifier.send_ses")
    def test_uses_failure_template_on_validation_errors(self, send):
        self.ir.validation_errors.append(validation.ValidationResult(1))
        send_email(self.ctx, [], self.ir)
        send.assert_called_once()
        expect(send.call_args[0][2]).to(equal("Dioptra transaction import: Failure"))

    @mock.patch("dioptra.notifier.send_ses")
    def test_uses_missing_csv_on_csv_missing(self, send):
        self.ir.exception = missing_csv_error
        send_email(self.ctx, [], self.ir)
        send.assert_called_once()
        expect(send.call_args[0][2]).to(
            equal("Dioptra transaction import: Archive missing transactions.csv")
        )

    @mock.patch("dioptra.notifier.send_ses")
    def test_uses_exception_template_on_exception(self, send):
        self.ir.exception = ArithmeticError()
        send_email(self.ctx, [], self.ir)
        send.assert_called_once()
        expect(send.call_args[0][2]).to(equal("Dioptra transaction import: Unexpected Error"))

    def test_ses_calls_boto(self):
        self.ctx.config.from_email = "from@email.com"
        client = mock.MagicMock()
        with mock.patch("dioptra.notifier.ses_client", lambda *_: client):
            send_ses(self.ctx, ["hi@email.com"], "subj", "body")
        client.send_email.assert_called_once()
        expect(client.send_email.call_args[1]).to(
            equal(
                {
                    "Source": "from@email.com",
                    "Destination": {"ToAddresses": ["hi@email.com"]},
                    "Message": {
                        "Subject": {"Data": "subj", "Charset": "UTF-8"},
                        "Body": {"Text": {"Data": "body", "Charset": "UTF-8"}},
                    },
                    "ReplyToAddresses": ["from@email.com"],
                }
            )
        )


@mock.patch("dioptra.notifier.appconfig.build_time", "btime")
@mock.patch("dioptra.notifier.appconfig.build_sha", "bsha")
class TemplateTests(unittest.TestCase):
    def setUp(self):
        self.ctx = context.tester()
        self.ir = importer.ImportResult(1)
        self.ir.old_row_count = 123
        self.ir.new_row_count = 456
        self.ctx.stage_timing.set_transactions_file_last_modified(1000)
        self.ctx.stage_timing.set_import_finished(1078)

    def test_success(self):
        subject, body = success_template(self.ctx, self.ir)
        expect(subject).to(equal("Dioptra transaction import: Success"))
        expect(body).to(
            equal(
                """Dioptra successfully loaded 456 transactions to the data store.

There were 123 transactions in the data store at the time the job started.
The transaction data store now has 456 transactions.
The job took 1 minute 18 seconds to run.


Pipeline build bsha at btime
"""
            )
        )

    def test_success_append_mode(self):
        self.ctx.config.append_mode = True
        subject, body = success_template(self.ctx, self.ir)
        expect(subject).to(equal("Dioptra transaction import: Success"))
        expect(body).to(
            equal(
                """Dioptra successfully loaded 333 additional transactions to the data store.

There were 123 transactions in the data store at the time the job started.
The transaction data store now has 456 transactions.
The job took 1 minute 18 seconds to run.


Pipeline build bsha at btime
"""
            )
        )

    def test_validation_errors(self):
        # Ensure errors are sorted by row
        self.ir.validation_errors.append(validation.ValidationResult(4))
        self.ir.validation_errors.append(validation.ValidationResult(2))
        self.ir.validation_errors.append(validation.ValidationResult(1))
        self.ir.validation_errors.append(validation.ValidationResult(3))
        for e in self.ir.validation_errors:
            e.add_error("hi")
        subject, body = validation_errors_template(self.ctx, self.ir)
        expect(subject).to(equal("Dioptra transaction import: Failure"))
        expect(body).to(
            equal(
                """The following 4 errors were found when trying to load the transaction data:

- Row 1: hi
- Row 2: hi
- Row 3: hi
- Row 4: hi

There were 123 transactions in the data store at the time the job started.
The transactions were unchanged because the job failed.
The job took 1 minute 18 seconds to run.


Pipeline build bsha at btime
"""
            )
        )

    def test_validation_errors_capped_at_configured_value(self):
        # Ensure capping is done after sorting
        self.ir.validation_errors.append(validation.ValidationResult(2))
        for i in range(10):
            self.ir.validation_errors.append(
                validation.ValidationResult(i + 4 + random.randint(0, 20))
            )
        self.ir.validation_errors.append(validation.ValidationResult(3))
        self.ir.validation_errors.append(validation.ValidationResult(1))

        for e in self.ir.validation_errors:
            e.add_error("hi")
        self.ctx.config.max_validation_errors = 3
        subject, body = validation_errors_template(self.ctx, self.ir)
        expect(subject).to(equal("Dioptra transaction import: Failure"))
        expect(body).to(
            equal(
                """At least 13 errors were found trying to load the transaction data.
Here are the first 3:

- Row 1: hi
- Row 2: hi
- Row 3: hi

There were 123 transactions in the data store at the time the job started.
The transactions were unchanged because the job failed.
The job took 1 minute 18 seconds to run.


Pipeline build bsha at btime
"""
            )
        )

    def test_missing_csv(self):
        self.ir.exception = missing_csv_error
        zi1 = zipfile.ZipInfo("not-the-droid")
        zi1.file_size = 25
        zi2 = zipfile.ZipInfo("jedi-master")
        zi2.file_size = 30
        self.ir.zip_infos.append(zi1)
        self.ir.zip_infos.append(zi2)
        expect(self.ir.missing_transactions_csv).to(be_true)
        subject, body = missing_csv_template(self.ctx, self.ir)
        expect(subject).to(
            equal("Dioptra transaction import: Archive missing transactions.csv")
        )
        expect(body).to(
            equal(
                f"""The uploaded transactions.csv.zip file uploaded was missing a transactions.csv file.
The contents of the archive are:

- not-the-droid (25 bytes)
- jedi-master (30 bytes)


Pipeline build bsha at btime
"""
            )
        )

    def test_missing_csv_empty(self):
        self.ir.exception = missing_csv_error
        subject, body = missing_csv_template(self.ctx, self.ir)
        expect(subject).to(
            equal("Dioptra transaction import: Archive missing transactions.csv")
        )
        expect(body).to(
            equal(
                f"""The uploaded transactions.csv.zip file uploaded was missing a transactions.csv file.
The archive was empty.


Pipeline build bsha at btime
"""
            )
        )

    def test_exception(self):
        self.ir.exception = ArithmeticError("hi")
        self.ir.reported_exception_id = "myerrorid"
        subject, body = exception_template(self.ctx, self.ir)
        expect(subject).to(equal("Dioptra transaction import: Unexpected Error"))
        expect(body).to(
            equal(
                f"""An unexpected error occurred when trying to load the transaction data.

There were 123 transactions in the data store at the time the job started.
The transactions were unchanged because the job failed.
The job took 1 minute 18 seconds to run.

* Error ID myerrorid
* Trace ID {self.ctx.trace_id}


Pipeline build bsha at btime
"""
            )
        )

    def fmt(self, secs):
        return format_duration(datetime.timedelta(seconds=secs))

    def test_format_duration_singular_plural(self):
        expect(self.fmt(61)).to(equal("1 minute 1 second"))
        expect(self.fmt(122)).to(equal("2 minutes 2 seconds"))
        expect(self.fmt(0)).to(equal("0 seconds"))

    def test_format_duration_missing_parts(self):
        expect(self.fmt(60)).to(equal("1 minute"))
        expect(self.fmt(40)).to(equal("40 seconds"))

    def test_format_duration_fractional(self):
        expect(self.fmt(0)).to(equal("0 seconds"))
        expect(self.fmt(0.4)).to(equal("0 seconds"))
        expect(self.fmt(0.6)).to(equal("1 second"))
        expect(self.fmt(1.3)).to(equal("1 second"))
        expect(self.fmt(59.3)).to(equal("59 seconds"))
        expect(self.fmt(59.8)).to(equal("1 minute"))
        expect(self.fmt(60.1)).to(equal("1 minute"))
        expect(self.fmt(60.6)).to(equal("1 minute 1 second"))
        expect(self.fmt(122.45678)).to(equal("2 minutes 2 seconds"))
