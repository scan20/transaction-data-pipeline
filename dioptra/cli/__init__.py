import csv
import random
import zipfile

import click
import datetime


@click.group()
@click.pass_context
def cli(ctx):
    """
    Choose from a suite of available analyzers to run and report on.
    """
    ctx.ensure_object(dict)


@cli.command(
    "build-test-data",
    help="Build a test transactions csv/zip file with the configured rows",
)
@click.option("--out", required=True)
@click.option("--zipped")
@click.option("--start-date", default="2016-01-01")
@click.option("--end-date", default="2020-12-31")
@click.option("--rows-per-day", type=int, default=600)
@click.option("--country-code", default="BD")
@click.option("--grant-code", default="DB2021")
@click.option("--budget-line-code", default="BPH01")
@click.option(
    "--account-code",
    type=int,
    default=500,
    help="Starting code, modify --rows-per-account to make larger or smaller variations.",
)
@click.option("--rows-per-account", type=int, default=2000)
@click.option("--site-code", default="AMM")
@click.option("--sector-code", default="OADM")
@click.option("--transaction-code", default="JOD/CD10687")
@click.option("--currency", default="JOD")
@click.option("--max_rows", type=int, default=1000000)
def build_test_data(
    out,
    zipped,
    start_date,
    end_date,
    rows_per_day,
    country_code,
    grant_code,
    budget_line_code,
    account_code,
    rows_per_account,
    site_code,
    sector_code,
    transaction_code,
    currency,
    max_rows,
):
    start_date = datetime.datetime.strptime(start_date, "%Y-%m-%d").date()
    end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d").date()
    assert end_date > start_date

    rows = 0
    outf = open(out, "w")
    today = start_date
    writer = csv.writer(outf)
    while today <= end_date and rows < max_rows:
        todaystr = today.isoformat()
        for i in range(rows_per_day):
            # Example row is:
            # 2019-04-30,5YE,TFI26,TFA00001,70000,9OEC,MMUA,HQPR APR 19,HQPR APR 19,USD,TU Salaries,1958.64
            row = [
                todaystr,
                country_code,
                grant_code,
                budget_line_code,
                account_code,
                site_code,
                sector_code,
                transaction_code,
                f"Test transaction description {i}",
                currency,
                f"Test budget line desc {i}",
                round(random.randint(0, 999) + random.random(), 2),
            ]
            writer.writerow(row)
            rows += 1
            if rows % rows_per_account == 0:
                account_code += 1
        today = today + datetime.timedelta(days=1)

    outf.flush()
    outf.close()

    if zipped:
        with open(out) as csv_f:
            with open(zipped, "wb") as zip_f:
                with zipfile.ZipFile(zip_f, "w", zipfile.ZIP_STORED) as myzip:
                    myzip.writestr("transactions.csv", csv_f.read())
