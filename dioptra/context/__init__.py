import hashlib
import sentry_sdk
import shortuuid

from dioptra import appconfig, timing

sentry_initialized = False

if appconfig.sentry_dsn:
    sentry_sdk.init(dsn=appconfig.sentry_dsn)
    sentry_sdk.set_tag("build_sha", appconfig.build_sha)
    sentry_sdk.set_tag("build_time", appconfig.build_time)
    sentry_initialized = True


class Context(object):
    """Context represents a single run through the pipeline,
    or an import, etc. It's similar to a web request context,
    that follows a request through the system, except we want to pass this explicitly
    and not use threadlocals or anything magical.

    This ensures the logger has a consistent id, we only have one config,
    and we have a unique ID for other uses too (like if we need
    to make external requests and want to add a trace ID).

    NOTE: When running tests, use the tester() function,
    rather than creating this directly.
    It will use test databases.
    """

    running_tests: bool = False
    config: appconfig.Config
    trace_id: str
    logger = None
    stage_timing: timing.Stages
    perf_agg: timing.PerfAgg

    def __init__(self):
        self.running_tests = False

    def _init_new(self, cfg):
        self.config = cfg
        self.trace_id = shortuuid.uuid()
        self.logger = appconfig.get_logger().bind(trace_id=self.trace_id)
        self.stage_timing = timing.Stages()
        self.perf_agg = timing.PerfAgg()

    def bind(self, **log_fields) -> "Context":
        result = Context()
        result.config = self.config
        result.running_tests = self.running_tests
        result.trace_id = self.trace_id
        result.logger = self.logger.bind(**log_fields)
        result.stage_timing = self.stage_timing
        result.perf_agg = self.perf_agg
        return result

    def capture_exception(self, error: BaseException):
        """If sentry is configured, call capture_exception.
        If it's not, log the error.
        """
        error_id = shortuuid.uuid()
        tags = {
            "error_id": error_id,
            "error_type": hashlib.md5(str(error).encode()).hexdigest(),
            "trace_id": self.trace_id,
        }

        if not sentry_initialized or self.running_tests:
            self.logger.bind(error=error, **tags).error("capture_exception")
        else:
            with sentry_sdk.configure_scope() as scope:
                for k, v in tags.items():
                    scope.set_tag(k, v)
                sentry_sdk.capture_exception(error)
        return error_id


def from_config(config: appconfig.Config) -> Context:
    ctx = Context()
    ctx._init_new(config)  # noqa
    return ctx


def tester() -> Context:
    ctx = Context()
    ctx._init_new(appconfig.Config())  # noqa
    ctx.running_tests = True
    ctx.config.max_database_connections = min([4, ctx.config.max_database_connections])
    ctx.config.s3_bucket = appconfig.default_s3_bucket
    ctx.config.s3_archive_key = appconfig.default_s3_archive_key
    ctx.config.s3_config_key = appconfig.default_s3_config_key
    ctx.config.append_mode = False
    return ctx
