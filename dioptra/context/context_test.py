import unittest
from unittest import mock
import structlog
from expects import *

from .. import context, testutils


@mock.patch("sentry_sdk.capture_exception")
class ContextCaptureExceptionTests(unittest.TestCase):
    @mock.patch("dioptra.context.sentry_initialized", False)
    def test_capture_exception_logs_if_sentry_not_initialized(self, sdkcap):
        ex = Exception("Hello")
        err_keys = {"error": ex, "error_type": "8b1a9953c4611296a827abf8c47804d7"}
        with testutils.assert_log_event("capture_exception", **err_keys):
            errid = context.tester().capture_exception(ex)
        sdkcap.assert_not_called()
        expect(errid).to_not(be_empty)

    @mock.patch("dioptra.context.sentry_initialized", True)
    def test_capture_exception_logs_if_running_tests(self, sdkcap):
        with testutils.assert_log_event("capture_exception"):
            context.tester().capture_exception(Exception("Hello"))
        sdkcap.assert_not_called()

    @mock.patch("dioptra.context.sentry_initialized", True)
    def test_capture_exception_uses_sentry_if_configured_and_not_testing(self, sdkcap):
        with structlog.testing.capture_logs() as logs:
            ctx = context.tester()
            ctx.running_tests = False
            errid = ctx.capture_exception(Exception("Hello"))
        expect(logs).to(be_empty)
        sdkcap.assert_called_once()
        expect(errid).to_not(be_empty)
