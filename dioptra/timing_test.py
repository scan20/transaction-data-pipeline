import datetime
import unittest
import pytz

from expects import *

from . import timing


class TimingStagesTests(unittest.TestCase):
    def test_helper_calculations(self):
        st = timing.Stages()
        st.set_transactions_file_last_modified(datetime.datetime.fromtimestamp(5000))
        st.set_pipeline_started(datetime.datetime.fromtimestamp(6000))
        st.set_import_started(datetime.datetime.fromtimestamp(6500))
        st.set_import_finished(datetime.datetime.fromtimestamp(6750))

        expect(st.total_duration).to(equal(datetime.timedelta(seconds=1750)))
        expect(st.waiting_for_start).to(equal(datetime.timedelta(seconds=1000)))
        expect(st.importer_prep).to(equal(datetime.timedelta(seconds=500)))
        expect(st.importer).to(equal(datetime.timedelta(seconds=250)))
        expect(st.total_importer).to(equal(datetime.timedelta(seconds=750)))

    def test_helpers_with_empty_values(self):
        st = timing.Stages()

        def expect_all_zero():
            expect(st.total_duration).to(equal(datetime.timedelta(0)))
            expect(st.waiting_for_start).to(equal(datetime.timedelta(0)))
            expect(st.importer_prep).to(equal(datetime.timedelta(0)))
            expect(st.importer).to(equal(datetime.timedelta(0)))
            expect(st.total_importer).to(equal(datetime.timedelta(0)))

        expect_all_zero()
        st.reset()
        expect_all_zero()

    naive_dt = datetime.datetime(2019, 12, 9, 12, 11, 3)
    aware_dt = pytz.timezone("US/Central").localize(datetime.datetime(2019, 12, 9, 12, 11, 3))

    def test_is_naive_datetime(self):
        expect(timing.is_naive_datetime(self.naive_dt)).to(be_true)
        expect(timing.is_naive_datetime(self.aware_dt)).to(be_false)

    def test_handles_tz_awareness(self):
        st = timing.Stages()
        st.set_transactions_file_last_modified(
            pytz.timezone("US/Eastern").localize(self.naive_dt)
        )
        st.set_import_finished(self.aware_dt)
        expect(st.total_duration).to(equal(datetime.timedelta(hours=1)))

    def test_localizes_timestamps(self):
        st = timing.Stages()
        st.set_import_finished(self.naive_dt)
        expect(st._import_finished.tzinfo).to(equal(timing._tzaware.tzinfo))

        st.set_import_finished(self.aware_dt)
        expect(st._import_finished.strftime("%Y-%m-%d %H:%M:%S %Z (%z)")).to(
            equal("2019-12-09 12:11:03 CST (-0600)")
        )
