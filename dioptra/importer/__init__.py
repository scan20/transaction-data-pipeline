import multiprocessing.pool
import os
import zipfile
from io import TextIOWrapper
from typing import List, Generator
from zipfile import ZipFile

from .. import timing, validation, waitgroup, context
from . import worker, database


class ImportResult(object):
    rows_read: int = 0
    rows_processed: int = 0
    rows_written: int = 0
    old_row_count: int = 0
    new_row_count: int = 0
    validation_errors: List[validation.ValidationResult] = None
    zip_infos: List[zipfile.ZipInfo] = None
    exception: BaseException = None
    reported_exception_id: str = None

    def __init__(self, max_validation_errors: int):
        self._max_validation_errors = max_validation_errors
        self.validation_errors = []
        self.zip_infos = []

    def __repr__(self):
        return f"{type(self).__name__}({self.__dict__})"

    @property
    def exceeded_validation_errors(self):
        return len(self.validation_errors) >= self._max_validation_errors

    @property
    def stop_early(self):
        return self.exception is not None or self.exceeded_validation_errors

    @property
    def success(self):
        return self.exception is None and len(self.validation_errors) == 0

    @property
    def missing_transactions_csv(self):
        return (
            self.exception is not None
            and type(self.exception) == KeyError
            and self.exception.args
            == ("There is no item named 'transactions.csv' in the archive",)
        )


class Importer(object):
    """Import a transactions zip file.
    See README for more details about all the stuff that goes on.

    `zip_file` can be anything `zipfile.ZipFile accepts`:
    a filename/path-like or path-like object.
    So it can be a filename for local testing, or an S3 object stream when testing remotely.

    It must contain a `transactions.csv` entry.
    """

    def __init__(self, zip_file, ctx: context.Context):
        self.zip_file = zip_file
        self.ctx = ctx
        self.config = self.ctx.config
        if isinstance(zip_file, os.PathLike) or isinstance(zip_file, str):
            self.ctx = self.ctx.bind(zip_file=zip_file)
        else:
            self.ctx = self.ctx.bind(zip_file="<stream>")

        connection_pool = database.new_pool(
            self.config.postgres_url, self.config.max_database_connections
        )
        self.db_app = database.App(self.ctx, connection_pool)

        self.process_pool = multiprocessing.pool.ThreadPool(
            processes=self.config.thread_pool_size
        )
        self.finished_wg = waitgroup.WaitGroup()

        self.worker_state = worker.WorkerState(self.ctx, self.db_app)

    def run(self) -> ImportResult:
        self.ctx.stage_timing.set_import_started()
        result = ImportResult(self.config.max_validation_errors)
        perfmon = timing.Perfmon(
            self.ctx.logger,
            lambda: {
                "rows_read": result.rows_read,
                "rows_processed": result.rows_processed,
            },
            interval=self.config.perfmon_interval,
        )
        perfmon.start()
        try:
            with timing.log_timing(self.ctx.logger, "import_preprocessing"):
                self._preprocess()
            with timing.log_timing(self.ctx.logger, "import_processing"):
                self._process(result)
            with timing.log_timing(self.ctx.logger, "import_postprocessing"):
                self._postprocess(result)
        except Exception as ex:
            self.ctx.logger.bind(error=ex).error("import_master_error")
            result.exception = ex
        finally:
            if result.exception:
                result.reported_exception_id = self.ctx.capture_exception(result.exception)
            perfmon.cancel()
            self.ctx.stage_timing.set_import_finished()
        return result

    def _preprocess(self):
        self.db_app.ensure_tables()

    def _process(self, result: ImportResult):
        finished_wg = self.finished_wg

        def chunk_callback(wr: worker.WorkResult):
            result.rows_processed += wr.chunk_size
            result.rows_written += wr.rows_written
            finished_wg.done()
            result.validation_errors.extend(wr.validation_errors)

        def chunk_error(exc):
            result.exception = exc
            finished_wg.done()

        for chunk in self._yield_chunks(result):
            if result.stop_early:
                break
            self.finished_wg.incr()
            self.process_pool.apply_async(
                worker.process_chunk,
                [self.worker_state, chunk],
                callback=chunk_callback,
                error_callback=chunk_error,
            )

        self.finished_wg.wait()

    def _postprocess(self, result: ImportResult):
        """After processing, we:
        - Roll back on failure.
        - Roll forward on success
        - Append on success if append mode
        - Clear the DB on success if append mode and input was empty (handled implicitly if not append mode)
        """
        result.old_row_count = self.db_app.select_meta_count(self.db_app.meta_table)
        if not result.success:
            result.new_row_count = result.old_row_count
            self.db_app.roll_imported_tables_back()
            return

        if not self.config.append_mode:
            result.new_row_count = result.rows_written
            self.db_app.set_meta_count(self.db_app.next_meta_table, result.new_row_count)
            self.db_app.roll_imported_tables_forward()
            return

        # Append mode with empty file is special case, we want to clear the DB.
        if result.rows_written == 0:
            result.new_row_count = 0
            self.db_app.set_meta_count(self.db_app.next_meta_table, 0)
            self.db_app.roll_imported_tables_forward()
            return

        result.new_row_count = result.old_row_count + result.rows_written
        self.db_app.set_meta_count(self.db_app.next_meta_table, result.new_row_count)
        self.db_app.append_imported_table_forward()

    def _yield_chunks(self, result: ImportResult) -> Generator[worker.Chunk, None, None]:
        chunk_start_row_idx = 1
        with ZipFile(self.zip_file) as zf:
            result.zip_infos = zf.infolist()
            with zf.open("transactions.csv", "r") as infile:
                # It's possible that the csv file uses non-utf8 encoding.
                # In some cases, like for small archives, when loading the CSV directly,
                # or if the non-utf8 is at the beginning of the file,
                # this would usually lead to a UnicodeDecodeError.
                # However, in other cases, like for non-utf8 towards the end of a large file,
                # or if the characters occur in a certain way,
                # the invalid chars will lead to CSV parsing issues instead,
                # and we'll get something like reading the wrong number of columns.
                # To avoid this, we replace invalid characters
                # and then look for the unicode replacement character while doing validation.
                textio = TextIOWrapper(infile, "utf-8", errors="replace")
                # We want a file of only whitespace to count as a blank file (no rows).
                # Since newlines in CSV are significant, and we read lines in incrementally,
                # this takes some special-case handling.
                #
                # When we read the first chunk, if the entire thing is blank,
                # we assume the file is supposed to be blank and do not process the chunk.
                # If the file is larger than a chunk and still has whitespace/empty lines,
                # (or if that initial chunk is not entirely blank),
                # we will get validation errors, which seems desirable:
                # it seems a large blank file
                # is not what someone would expect to upload,
                # and we should be consistent with how CSVs are treated, with significant whitespace
                # (if we want to start coercing/cleaning up input, that should be tackled separately).
                first_chunk = True
                while True:
                    chunk_lines = textio.readlines(self.ctx.config.chunk_size)
                    chunk_len = len(chunk_lines)
                    if chunk_len == 0:
                        break

                    result.rows_read += chunk_len
                    if first_chunk and all(line.isspace() or not line for line in chunk_lines):
                        pass
                    else:
                        yield worker.Chunk(chunk_start_row_idx, chunk_lines)
                    first_chunk = False
                    chunk_start_row_idx += chunk_len
