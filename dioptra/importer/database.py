from contextlib import contextmanager
import datetime

import psycopg2cffi as psycopg2
from more_itertools import first
from psycopg2cffi import errorcodes
import psycopg2cffi.pool

from .. import context

PoolType = psycopg2cffi.pool.ThreadedConnectionPool


def new_pool(postgres_url: str, max_conns: int) -> PoolType:
    return psycopg2.pool.ThreadedConnectionPool(int(max_conns * 0.7), max_conns, postgres_url)


@contextmanager
def connection(pool: PoolType):
    conn = pool.getconn()
    if conn is None:
        raise Exception("unable to get a connection")
    try:
        yield conn
    finally:
        pool.putconn(conn)


@contextmanager
def transaction(pool: PoolType, commit=True):
    with connection(pool) as conn, conn.cursor() as cur:
        result = yield cur
        cur.close()
        if commit:
            conn.commit()
        return result


class App(object):
    def __init__(self, ctx: context.Context, pool: PoolType):
        self.ctx = ctx
        self.pool = pool
        if ctx.running_tests:
            self.imported_table = f'"test_transactions_{self.ctx.trace_id}"'
            self.meta_table = f'"test_transactions_meta_{self.ctx.trace_id}"'
            self.next_imported_table = f'"test_transactions_next_{self.ctx.trace_id}"'
            self.next_meta_table = f'"test_meta_next_{self.ctx.trace_id}"'
        else:
            self.imported_table = '"transactions"'
            self.meta_table = '"transactions_meta"'
            self.next_imported_table = f'"transactions_next_{self.ctx.trace_id}"'
            self.next_meta_table = f'"meta_next_{self.ctx.trace_id}"'

    def ensure_tables(self):
        with transaction(self.pool) as cur:
            cur.execute(self._schema_sql_statement())

    def count_slow(self, table):
        """SELECT COUNT(*) FROM <table>.
        This is a slow operation on big tables!
        Use the meta table instead during processing.
        """
        with transaction(self.pool, commit=False) as cur:
            cur.execute(f"SELECT COUNT(*) FROM {table}")
            return first(cur.fetchone() or [], 0)

    def copy_from_csv(self, column_count, csvio):
        """Use COPY to mass-import values into the transactions table.
        Because the number of columns in the CSV are variable,
        we need the caller to provide how many columns will be used,
        so we can generate the correct SQL.
        """
        with transaction(self.pool) as cur:
            with self.ctx.perf_agg.time("dbcopy"):
                colnames = ", ".join(_column_names[:column_count])
                cur.copy_expert(
                    f"COPY {self.next_imported_table} ({colnames}) FROM stdin WITH NULL AS '''' CSV",
                    csvio,
                )

    def set_meta_count(self, table, count):
        with transaction(self.pool) as cur:
            cur.execute(f"INSERT INTO {table} (row_count) VALUES (%s)", (count,))

    def select_meta_count(self, table):
        with transaction(self.pool, commit=False) as cur:
            try:
                cur.execute(f"SELECT row_count FROM {table}")
            except psycopg2.ProgrammingError as e:
                # Handle a broken schema by assuming an empty table (or -1 rather), it's fine.
                if (
                    e.pgcode == errorcodes.UNDEFINED_TABLE
                    or e.pgcode == errorcodes.UNDEFINED_COLUMN
                ):
                    return -1
                raise
            return first(cur.fetchone() or [], 0)

    def roll_imported_tables_forward(self):
        """On successful import, call this to rename the 'next' tables to the real tables."""
        with transaction(self.pool) as cur:
            cur.execute(f"DROP TABLE IF EXISTS {self.imported_table}")
            cur.execute(f"DROP TABLE IF EXISTS {self.meta_table}")
            cur.execute(
                f"ALTER TABLE {self.next_imported_table} RENAME TO {self.imported_table}"
            )
            cur.execute(f"ALTER TABLE {self.next_meta_table} RENAME TO {self.meta_table}")

    def append_imported_table_forward(self):
        """On successful import in append mode, call this to insert the 'next' table into
        the current transactions table, rename meta table, drop the unused stuff."""
        with transaction(self.pool) as cur:
            cur.execute(f"DROP TABLE IF EXISTS {self.meta_table}")
            cur.execute(f"ALTER TABLE {self.next_meta_table} RENAME TO {self.meta_table}")
            cur.execute(
                f"INSERT INTO {self.imported_table} (SELECT * FROM {self.next_imported_table})"
            )
            cur.execute(f"DROP TABLE {self.next_imported_table}")

    def roll_imported_tables_back(self):
        """On unsuccessful import, clean up the old tables."""
        with transaction(self.pool) as cur:
            cur.execute(f"DROP TABLE {self.next_imported_table}")
            cur.execute(f"DROP TABLE {self.next_meta_table}")

    def _schema_sql_statement(self):
        created = datetime.datetime.utcnow().isoformat()
        return f"""
-- Meta table should always exist; this schema can be added to safely and it'll be
-- copied over to the next meta table.
CREATE TABLE IF NOT EXISTS {self.meta_table} (
    row_count INTEGER NOT NULL DEFAULT 0
);
COMMENT ON TABLE {self.meta_table} IS 'Created {created}';

CREATE TABLE {self.next_meta_table} (LIKE {self.meta_table});
COMMENT ON TABLE {self.next_meta_table} IS 'Created {created}';

CREATE TABLE {self.next_imported_table} (
    transaction_date            DATE NOT NULL,
    country_code                TEXT NOT NULL DEFAULT '',
    grant_code                  TEXT NOT NULL DEFAULT '',
    budget_line_code            TEXT NOT NULL DEFAULT '',
    account_code                TEXT NOT NULL DEFAULT '',
    site_code                   TEXT NOT NULL DEFAULT '',
    sector_code                 TEXT NOT NULL DEFAULT '',
    transaction_code            TEXT NOT NULL DEFAULT '',
    transaction_description     TEXT NOT NULL DEFAULT '',
    currency_code               VARCHAR(4) NOT NULL DEFAULT '',
    budget_line_description     TEXT NOT NULL DEFAULT '',
    amount                      DECIMAL NOT NULL,
    dummy_field_1               TEXT NOT NULL DEFAULT '',
    dummy_field_2               TEXT NOT NULL DEFAULT '',
    dummy_field_3               TEXT NOT NULL DEFAULT '',
    dummy_field_4               TEXT NOT NULL DEFAULT '',
    dummy_field_5               TEXT NOT NULL DEFAULT ''
);
COMMENT ON TABLE {self.next_imported_table} IS 'Created {created}';

-- Need transactions table to exist for initial run purposes during testing, fresh db, etc.
CREATE TABLE IF NOT EXISTS {self.imported_table} (LIKE {self.next_imported_table});
COMMENT ON TABLE {self.imported_table} IS 'Created {created}';
"""


_column_names = [
    "transaction_date",
    "country_code",
    "grant_code",
    "budget_line_code",
    "account_code",
    "site_code",
    "sector_code",
    "transaction_code",
    "transaction_description",
    "currency_code",
    "budget_line_description",
    "amount",
    "dummy_field_1",
    "dummy_field_2",
    "dummy_field_3",
    "dummy_field_4",
    "dummy_field_5",
]
