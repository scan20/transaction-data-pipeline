import sys

from dioptra import appconfig, context
from dioptra.importer import Importer
from dioptra.testutils import zip_transactions


def get_zip(f):
    if f.endswith(".zip"):
        return f
    with open(f, "r") as csv:
        return zip_transactions(csv.read())


if __name__ == "__main__":
    appconfig.logconfig.configure()
    ctx = context.from_config(appconfig.Config())
    ctx.stage_timing.reset()

    res = Importer(get_zip(sys.argv[1]), ctx).run()
    ctx.logger.bind(**ctx.perf_agg.log_fields()).info("perfagg")
    ctx.logger.bind(
        rows_read=res.rows_read,
        rows_processed=res.rows_processed,
        rows_written=res.rows_written,
        old_row_count=res.old_row_count,
        new_row_count=res.new_row_count,
        duration=ctx.stage_timing.total_importer.total_seconds(),
        validation_errors=len(res.validation_errors),
        error=res.exception,
        exceeded_validation_errors=res.exceeded_validation_errors,
        stop_early=res.stop_early,
        success=res.success,
    ).info("import_main_finished")
    for err in res.validation_errors[:5]:
        print(err.full_message())
