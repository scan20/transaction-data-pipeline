from io import StringIO
from typing import List

from .. import validation, context, appconfig
from . import database


class WorkerState(object):
    def __init__(self, ctx: context.Context, db_app: database.App):
        self.ctx = ctx
        self.db_app = db_app


class Chunk(object):
    def __init__(self, starting_row_index, lines):
        self.starting_row_index = starting_row_index
        self.lines = lines

    def csv_rows(self):
        for line in appconfig.csv_reader(self.lines):
            yield line


class WorkResult(object):
    chunk_size: int
    rows_written: int
    validation_errors: List


def process_chunk(worker_state: WorkerState, chunk: Chunk) -> WorkResult:
    result = WorkResult()
    result.chunk_size = len(chunk.lines)
    result.validation_errors = []

    valid_lines = []
    column_count = None
    for idx, csv_row in enumerate(chunk.csv_rows()):
        with worker_state.ctx.perf_agg.time("validation"):
            validation_result = validation.validate_row(
                chunk.starting_row_index + idx, csv_row
            )
            if validation_result.valid():
                valid_lines.append(chunk.lines[idx])
                column_count = len(csv_row)
            else:
                result.validation_errors.append(validation_result)

    # See copy_from_csv for details on why we need column_count
    if column_count is not None:
        worker_state.db_app.copy_from_csv(column_count, StringIO("".join(valid_lines)))
    result.rows_written = len(valid_lines)
    return result
