import random
import unittest
from unittest import mock

from expects import *
import psycopg2cffi as psycopg2
from psycopg2cffi import errorcodes

from .. import testutils, appconfig, context
from . import database, Importer


class ImportTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.dbpool = database.new_pool(appconfig.Config().postgres_url, 1)

    def setUp(self):
        self.ctx = context.tester()
        self.ctx.config.max_database_connections = 2
        self.ctx.config.thread_pool_size = 1
        self.dbapp = database.App(self.ctx, self.dbpool)

    def import_rows(self, rows):
        csv = testutils.to_csv(rows)
        return self.import_csv_text(csv)

    def import_csv_text(self, csv):
        zipio = testutils.zip_transactions(csv)
        return Importer(zipio, self.ctx).run()

    def test_imports_valid_file_and_rolls_tables_forward(self):
        self.ctx.config.chunk_size = random.randint(1, 50)
        result = self.import_rows(testutils.rows(10))
        expect(result.exception).to(be_none)
        expect(self.dbapp.count_slow(self.dbapp.imported_table)).to(equal(10))
        expect(result.rows_read).to(equal(10))
        expect(result.rows_processed).to(equal(10))
        expect(result.rows_written).to(equal(10))
        expect(result.new_row_count).to(equal(10))
        expect(result.validation_errors).to(be_empty)

    def test_does_not_modify_real_tables_but_cleans_up_old_if_import_not_success(self):
        rows = testutils.rows(8)
        rows.append(testutils.invalid_row())
        result = self.import_rows(rows)
        expect(result).to(
            have_properties(
                rows_read=9,
                rows_processed=9,
                rows_written=8,
                old_row_count=0,
                new_row_count=0,
                validation_errors=have_len(1),
                exception=None,
                exceeded_validation_errors=False,
                stop_early=False,
                success=False,
            )
        )
        expect(self.dbapp.count_slow(self.dbapp.imported_table)).to(equal(0))
        try:
            self.dbapp.count_slow(self.dbapp.next_imported_table)
            raise Exception("should not hit there")
        except psycopg2.ProgrammingError as e:
            expect(e.pgcode).to(equal(errorcodes.UNDEFINED_TABLE))
            expect(e.pgerror).to(
                match('relation "test_transactions_next_[a-zA-Z0-9]+" does not exist')
            )

    def test_early_outs_when_invalid_entries_surpassed(self):
        self.ctx.config.chunk_size = 4
        self.ctx.config.max_validation_errors = 3
        result = self.import_rows([testutils.invalid_row() for _ in range(100)])
        expect(result).to(
            have_properties(
                old_row_count=0,
                new_row_count=0,
                validation_errors=have_len(be_above_or_equal(3)),
                exception=None,
                exceeded_validation_errors=True,
                stop_early=True,
                success=False,
            )
        )
        expect(self.dbapp.count_slow(self.dbapp.imported_table)).to(equal(0))

    def test_increments_invalid_rows_of_chunks(self):
        self.ctx.config.chunk_size = 4
        result = self.import_rows([testutils.invalid_row() for _ in range(10)])
        expect(result).to(have_property("success", False))
        expect(result.validation_errors[0].full_message()).to(start_with("Row 1:"))
        expect(result.validation_errors[-1].full_message()).to(start_with("Row 10:"))

    def test_reports_old_row_count(self):
        with database.transaction(self.dbpool) as cur:
            cur.execute(f"DROP TABLE IF EXISTS {self.dbapp.meta_table}")

        first_result = self.import_rows(testutils.rows(13))
        expect(first_result.exception).to(be_none)
        expect(first_result.old_row_count).to(equal(0))
        expect(first_result.new_row_count).to(equal(13))

        second_result = self.import_rows(testutils.rows(7))
        expect(second_result.exception).to(be_none)
        expect(second_result.old_row_count).to(equal(13))
        expect(second_result.new_row_count).to(equal(7))

    def test_early_outs_on_preprocessing_exception(self):
        self.ctx.config.max_database_connections = 0
        result = self.import_rows([testutils.row()])
        expect(result).to(
            have_properties(
                rows_written=0,
                stop_early=True,
                success=False,
            )
        )
        expect(str(result.exception)).to(
            equal("connection pool exausted")
        )  # check out that typo!

    def test_early_outs_on_worker_exception(self):
        with mock.patch("dioptra.importer.database.App.copy_from_csv") as func:
            func.side_effect = ArithmeticError("test err")
            result = self.import_rows([testutils.row()])
        expect(result).to(
            have_properties(
                rows_written=0,
                stop_early=True,
                success=False,
                exception=be_an(ArithmeticError),
            )
        )

    def test_early_outs_on_error_reading_zip(self):
        result = Importer("not/a/file", self.ctx).run()
        expect(result).to(
            have_properties(
                rows_written=0,
                stop_early=True,
                success=False,
                exception=be_an(FileNotFoundError),
            )
        )

    def test_treats_blank_file_as_no_rows(self):
        result = self.import_csv_text("")
        expect(result).to(
            have_properties(success=True, rows_read=0, rows_processed=0, rows_written=0)
        )

    def test_treats_whitespace_file_as_no_rows(self):
        result = self.import_csv_text("\n  \n\n\t\n")
        expect(result).to(
            have_properties(success=True, rows_read=4, rows_processed=0, rows_written=0)
        )

    def test_errors_for_any_empty_lines_in_non_empty_file(self):
        leading = self.import_rows([[], testutils.row(0)])
        expect(leading).to(have_properties(success=False, validation_errors=have_len(1)))
        expect(leading.validation_errors[0].full_message()).to(
            start_with("Row 1: 12 to 17 columns required (got 0)")
        )

        trailing = self.import_rows([testutils.row(0), []])
        expect(trailing).to(have_properties(success=False, validation_errors=have_len(1)))

        inside = self.import_rows([testutils.row(0), [], testutils.row(0)])
        expect(inside).to(have_properties(success=False, validation_errors=have_len(1)))

    def test_errors_if_rows_start_after_full_page_of_whitespace(self):
        self.ctx.config.chunk_size = 2
        csv = "     \n" * 2000
        csv += testutils.to_csv(testutils.rows(2))
        result = self.import_csv_text(csv)
        expect(result).to(have_properties(validation_errors=have_len(be_above_or_equal(50))))
        expect(result.validation_errors[0].full_message()).to(
            start_with("Row 2: 12 to 17 columns required (got 1)")
        )

    def test_reports_exception_to_sentry(self):
        with testutils.assert_log_event("capture_exception"):
            result = Importer("not/a/file", context.tester()).run()
        expect(result).to(
            have_properties(
                exception=be_an(FileNotFoundError), reported_exception_id=be_a(str)
            )
        )

    def test_handles_non_utf8_csv(self):
        result = Importer(testutils.testdata("non-utf8.csv.zip"), self.ctx).run()
        expect(result).to(
            have_properties(
                validation_errors=have_len(be_above_or_equal(1)),
                exception=None,
                success=False,
            )
        )
        expect(result.validation_errors[0].full_message()).to(
            start_with("Row 1: Contains invalid characters")
        )

    def test_reads_zip_infos(self):
        result = self.import_rows(testutils.rows(1))
        expect(result).to(
            have_properties(
                zip_infos=contain_exactly(have_properties(filename="transactions.csv")),
            )
        )
        expect(result.missing_transactions_csv).to(be_false)

    def test_knows_if_csv_missing(self):
        result = Importer(
            testutils.zip_transactions("this is some data", "foo/not-transactions.txt"),
            self.ctx,
        ).run()
        expect(result).to(
            have_properties(
                zip_infos=contain_exactly(
                    have_properties(filename="foo/not-transactions.txt", file_size=17)
                ),
            )
        )
        expect(result.missing_transactions_csv).to(be_true)

    def test_append_mode_adds_transactions_to_existing_store(self):
        self.insert_rows(10)

        self.ctx.config.append_mode = True
        ir2 = self.import_rows(testutils.rows(4))
        expect(self.dbapp.count_slow(self.dbapp.imported_table)).to(equal(14))
        expect(self.dbapp.select_meta_count(self.dbapp.meta_table)).to(equal(14))
        expect(ir2).to(
            have_properties(
                success=True,
                rows_read=4,
                rows_processed=4,
                rows_written=4,
                old_row_count=10,
                new_row_count=14,
            )
        )

    def test_append_mode_works_from_a_fresh_database(self):
        self.ctx.config.append_mode = True

        expect(self.import_rows(testutils.rows(10))).to(have_property("success", True))
        expect(self.dbapp.count_slow(self.dbapp.imported_table)).to(equal(10))
        expect(self.dbapp.select_meta_count(self.dbapp.meta_table)).to(equal(10))

        expect(self.import_rows(testutils.rows(15))).to(have_property("success", True))
        expect(self.dbapp.count_slow(self.dbapp.imported_table)).to(equal(25))
        expect(self.dbapp.select_meta_count(self.dbapp.meta_table)).to(equal(25))

    def insert_rows(self, n):
        ir = self.import_rows(testutils.rows(n))
        expect(ir).to(have_property("success", True))
        expect(self.dbapp.count_slow(self.dbapp.imported_table)).to(equal(n))

    def test_append_mode_replaces_db_for_empty_file(self):
        self.insert_rows(10)

        self.ctx.config.append_mode = True
        ir = self.import_csv_text("")
        expect(ir).to(
            have_properties(
                success=True,
                rows_read=0,
                rows_processed=0,
                rows_written=0,
                old_row_count=10,
                new_row_count=0,
            )
        )
        expect(self.dbapp.count_slow(self.dbapp.imported_table)).to(equal(0))
        expect(self.dbapp.select_meta_count(self.dbapp.meta_table)).to(equal(0))

    def test_append_mode_replaces_db_for_whitespace_file(self):
        self.insert_rows(10)

        self.ctx.config.append_mode = True
        ir = self.import_csv_text("  \n")
        expect(ir).to(
            have_properties(
                success=True,
                old_row_count=10,
                new_row_count=0,
            )
        )
        expect(self.dbapp.count_slow(self.dbapp.imported_table)).to(equal(0))
        expect(self.dbapp.select_meta_count(self.dbapp.meta_table)).to(equal(0))

    def test_append_mode_does_not_change_original_store_on_error(self):
        self.insert_rows(10)

        rows = testutils.rows(8)
        rows.append(testutils.invalid_row())
        result = self.import_rows(rows)
        expect(result).to(
            have_properties(
                rows_read=9,
                rows_processed=9,
                rows_written=8,
                old_row_count=10,
                new_row_count=10,
                validation_errors=have_len(1),
                success=False,
            )
        )
        expect(self.dbapp.count_slow(self.dbapp.imported_table)).to(equal(10))
        expect(self.dbapp.select_meta_count(self.dbapp.meta_table)).to(equal(10))
        try:
            self.dbapp.count_slow(self.dbapp.next_imported_table)
            raise Exception("should not hit there")
        except psycopg2.ProgrammingError as e:
            expect(e.pgcode).to(equal(errorcodes.UNDEFINED_TABLE))
            expect(e.pgerror).to(
                match('relation "test_transactions_next_[a-zA-Z0-9]+" does not exist')
            )
