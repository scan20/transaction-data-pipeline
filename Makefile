.DEFAULT_GOAL := help

ifneq (,$(wildcard ./.env))
    # If .env exists, include it and export it to make existing overrides available to Make.
    # Pass it through to docker as well, via an --env-file param.
    include .env
    export
    ENV_FILE_PARAM = --env-file .env
endif

DB_URL ?= postgres://dioptra:dioptra@db:5432/dioptra
BUILD_SHA := $(shell git describe --no-match --always --dirty)
BUILD_TIME := $(shell date +%Y-%m-%dT%T%z)

# Only use -t for docker-compose if we're in an interactive shell.
INTERACTIVE:=$(shell [ -t 0 ] && echo 1)
ifdef INTERACTIVE
TTY := -t
else
TTY :=
endif

DOCKER_BUILD_ARGS :=  --build-arg BUILD_SHA="${BUILD_SHA}" --build-arg BUILD_TIME="${BUILD_TIME}"

DOCKER_RUN_PREAMBLE := docker run --rm -i \
	$(TTY) \
	--network=dioptra \
	$(ENV_FILE_PARAM) \
	-v $(PWD):/app/

DOCKER_RUN := $(DOCKER_RUN_PREAMBLE) \
	-e DATABASE_URL=$(DB_URL) \
	-e INTERACTIVE=$(INTERACTIVE) \
	dioptra-devel

DOCKER_RUN_PGCLI := docker run --rm -it --network=dioptra dencold/pgcli

# These AWS_ variables must be present in your environment or in .env
DOCKER_RUN_AWSCLI := docker run --rm -it \
	-e AWS_DEFAULT_REGION=$(AWS_DEFAULT_REGION) \
	-e AWS_ACCESS_KEY_ID=$(AWS_ACCESS_KEY_ID) \
	-e AWS_SECRET_ACCESS_KEY=$(AWS_SECRET_ACCESS_KEY) \
	-e AWS_SESSION_TOKEN=$(AWS_SESSION_TOKEN) \
	-v $(PWD):/tmp \
	-w /tmp \
	ombu/docker-awscli

AWS := $(DOCKER_RUN_AWSCLI) aws
AWSLOGS := $(DOCKER_RUN_AWSCLI) awslogs

AWS_ACCT_NUMBER := $$($(AWS) sts get-caller-identity --output text --query 'Account' | tr -d '\r')
ECR_REPO_URI := $$($(AWS) ecr describe-repositories --repository-names dioptra/transaction-pipeline --output text --query 'repositories[0].repositoryUri' | tr -d '\r')

# Create an EC2 keypair and return the private key (one per region)
EC2_KEY = $$($(AWS) ec2 create-key-pair --key-name dioptra-bastion-$(AWS_DEFAULT_REGION)-$(AWS_ACCT_NUMBER) --output text --query 'KeyMaterial' | tr -d '\r')

# Public IP address for the bastion host (one per stack)
BASTION_IP = $$($(AWS) cloudformation describe-stacks --stack-name $(STACK) --query "Stacks[0].Outputs[?OutputKey=='BastionHostPublicIP'].OutputValue" --output text | tr -d '\r')

# Cluster endpoint for the RDS database (one per stack)
RDS_DB_HOST = $$($(AWS) cloudformation describe-stacks --stack-name $(STACK) --query "Stacks[0].Outputs[?OutputKey=='DBHost'].OutputValue" --output text | tr -d '\r')

# Bucket for the Cloudformation package artifacts and the sample test transactions
# (one per AWS account)
S3_BASE_BUCKET = cloudformation-dioptra-$(AWS_ACCT_NUMBER)

# Bucket for stack config.yaml and transactions.csv.zip (one per stack)
S3_STACK_TRANSACTIONS_BUCKET := $$($(AWS) cloudformation describe-stacks --stack-name $(STACK) --query "Stacks[0].Outputs[?OutputKey=='TransactionsBucket'].OutputValue" --output text | tr -d '\r')

NOTIFICATION_EMAILS ?= ['rob@lithic.tech','matt@lithic.tech', 'martin@ombuweb.com']

help:
	@grep -hE '^[a-zA-Z0-9_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build: build-devel-image  ## Build the docker development container image.

build-devel-image:
	docker build ${DOCKER_BUILD_ARGS} --build-arg REQUIREMENTS=development -t dioptra-devel -f Dockerfile .

install:
	@echo "You probably mean 'make build', deps are built into the docker image."
	exit 1

build-deploy-image: ## Build the docker deployment container image.
	docker build ${DOCKER_BUILD_ARGS} -t dioptra -f Dockerfile .

shell: ## Launch a shell in the development container.
	@$(DOCKER_RUN) bash

shell-test: ## Launch a shell in the development container, configured for the test database.
	@$(DOCKER_RUN) bash

up: ## Start the docker-compose services.
	docker-compose up -d

stop: ## Stop the docker-compose services.
	docker-compose stop

down: ## Destroy the docker-compose services.
	docker-compose down

psql: ## Launch psql, pointing at the development database.
	psql postgres://dioptra:dioptra@127.0.0.1:9005/dioptra

fmt: ## Format the source code with black.
	@docker run --rm -v "${PWD}":/data cytopia/black dioptra

lint: ## Run the flake8 linter on the codebase.
	@$(DOCKER_RUN) flake8 .

gen-test-data: ## Generate test data. Modify params to generate different sizes.
	@echo "This does not work in MacOS for some reason, see https://github.com/docker/for-mac/issues/5139"
	@echo "Uncomment and use at your own risk (or not on MacOS)!"
	#$(DOCKER_RUN) pypy3 -m dioptra.cli build-test-data --out=temp/transactions.csv --zipped=temp/transactions.csv.zip

test: ## Run the test suite.
	@$(DOCKER_RUN) pypy3 -m unittest discover -s . -p '*_test.py'

testimport-transactionscsv: ## Run an import against temp/transactions.csv, useful for debugging real data.
	@$(DOCKER_RUN) pypy3 -m dioptra.importer temp/transactions.csv

testimport-transactionszip: ## Run an import against temp/transactions.csv.zip, useful for debugging real data.
	@$(DOCKER_RUN) pypy3 -m dioptra.importer temp/transactions.csv.zip

testimport-1: ## Run a single-record import pipeline against the test database.
	@$(DOCKER_RUN) pypy3 -m dioptra.importer temp/transactions.1.csv.zip

testimport-100k: ## Run a 100k import pipeline against the test database.
	@$(DOCKER_RUN) pypy3 -m dioptra.importer temp/transactions.100K.csv.zip

testimport-1m: ## Run a 1M import pipeline against the test database.
	@$(DOCKER_RUN) pypy3 -m dioptra.importer temp/transactions.1M.csv.zip

testimport-10m: ## Run a 10M import pipeline against the test database.
	@$(DOCKER_RUN) pypy3 -m dioptra.importer temp/transactions.10M.csv.zip

testimport-30m: ## Run a 30M import pipeline against the test database.
	@$(DOCKER_RUN) pypy3 -m dioptra.importer temp/transactions.30M.csv.zip

testfullimport: ## Run the full import pipeline against the development database.
	@$(DOCKER_RUN) pypy3 -m dioptra.pipeline

emplace-pipeline-config: guard-STACK ## [STACK] Push a valid config.yml file to the S3 bucket for the STACK.
	@mkdir -p temp
	@echo "notification_emails: $(NOTIFICATION_EMAILS)" > temp/temp-pipe-config.yml
	@$(AWS) s3 cp temp/temp-pipe-config.yml s3://$(S3_STACK_TRANSACTIONS_BUCKET)/config.yml
	@rm temp/temp-pipe-config.yml

emplace-test-transactions: ## Upload sample transaction zip files in temp dir to S3.
	@$(AWS) s3 cp temp/transactions.1.csv.zip "s3://$(S3_BASE_BUCKET)/transactions.1.csv.zip"
	@$(AWS) s3 cp temp/transactions.100K.csv.zip "s3://$(S3_BASE_BUCKET)/transactions.100K.csv.zip"
	@$(AWS) s3 cp temp/transactions.1M.csv.zip "s3://$(S3_BASE_BUCKET)/transactions.1M.csv.zip"
	@$(AWS) s3 cp temp/transactions.10M.csv.zip "s3://$(S3_BASE_BUCKET)/transactions.10M.csv.zip"
	@$(AWS) s3 cp temp/transactions.30M.csv.zip "s3://$(S3_BASE_BUCKET)/transactions.30M.csv.zip"

triggerimport-1: guard-STACK ## [STACK] Trigger remote import of 1 row s3 file (see emplace-test-transactions).
	@$(AWS) s3 cp "s3://$(S3_BASE_BUCKET)/transactions.1.csv.zip" "s3://$(S3_STACK_TRANSACTIONS_BUCKET)/transactions.csv.zip"

triggerimport-100k: guard-STACK ## [STACK] Trigger remote import of 100k row s3 file (see emplace-test-transactions).
	@$(AWS) s3 cp "s3://$(S3_BASE_BUCKET)/transactions.100K.csv.zip" "s3://$(S3_STACK_TRANSACTIONS_BUCKET)/transactions.csv.zip"

triggerimport-1m: guard-STACK ## [STACK] Trigger remote import of 1 million row s3 file (see emplace-test-transactions).
	@$(AWS) s3 cp "s3://$(S3_BASE_BUCKET)/transactions.1M.csv.zip" "s3://$(S3_STACK_TRANSACTIONS_BUCKET)/transactions.csv.zip"

triggerimport-10m: guard-STACK ## [STACK] Trigger remote import of 10 million row s3 file (see emplace-test-transactions).
	@$(AWS) s3 cp "s3://$(S3_BASE_BUCKET)/transactions.10M.csv.zip" "s3://$(S3_STACK_TRANSACTIONS_BUCKET)/transactions.csv.zip"

triggerimport-30m: guard-STACK ## [STACK] Trigger remote import of 30 million row s3 file (see emplace-test-transactions).
	@$(AWS) s3 cp "s3://$(S3_BASE_BUCKET)/transactions.30M.csv.zip" "s3://$(S3_STACK_TRANSACTIONS_BUCKET)/transactions.csv.zip"

create-bastion-key-pair: ## Create an RSA key pair for accessing a bastion host. Run this once in each AWS account.
	@echo "$(EC2_KEY)" > ~/.ssh/dioptra-bastion-$(AWS_DEFAULT_REGION)-$(AWS_ACCT_NUMBER) && chmod 600 ~/.ssh/dioptra-bastion-$(AWS_DEFAULT_REGION)-$(AWS_ACCT_NUMBER)

create-s3-base-bucket: ## Create S3 base bucket. Run this once in each AWS account.
	@$(AWS) s3 mb s3://$(S3_BASE_BUCKET)

create-ecr-repo: ## Create ECR repo for dioptra/transaction-pipeline. Run this once in each AWS account.
	@$(AWS) ecr create-repository --repository-name dioptra/transaction-pipeline

put-ssm-param: guard-STACK guard-KEY guard-VALUE ## [STACK][KEY][VALUE] Set KEY=VALUE in SSM.
	@${AWS} ssm put-parameter --overwrite --type "String" \
		  --name "/${STACK}/${KEY}" \
		  --value "${VALUE}"
	@echo "${STACK}: ${KEY}=${VALUE}"

push-image: guard-APP_VERSION ## [APP_VERSION] Push the local dioptra image to ECR with a tag of APP_VERSION.
	@docker tag dioptra $(ECR_REPO_URI):$(APP_VERSION)
	@$(shell $(AWS) ecr get-login --no-include-email 2>/dev/null) 2>/dev/null
	@docker push $(ECR_REPO_URI):$(APP_VERSION)

init-cf-prereqs: guard-STACK ## [STACK] Take care of prerequisites before we can create STACK, like emplacing required SSM config.
	KEY=db-password VALUE="$(shell cat /dev/urandom | head -c 32 | shasum | head -c 32)" make put-ssm-param

package-cf-stack: guard-STACK ## [STACK] Package the cloudformation template for the STACK.
	@$(AWS) cloudformation package \
		--region=$(AWS_DEFAULT_REGION) \
		--template-file cloudformation/master.yaml \
		--s3-bucket $(S3_BASE_BUCKET) \
		--s3-prefix $(STACK) \
		--output-template-file cloudformation/built/$(STACK).yaml

deploy-cf-stack: guard-STACK guard-APP_VERSION ## [APP_VERSION][STACK] Deploy (create or update) the cloudformation STACK from local template.
	@$(AWS) cloudformation deploy \
		--stack-name $(STACK) \
		--template-file cloudformation/built/$(STACK).yaml \
		--capabilities CAPABILITY_NAMED_IAM \
		--parameter-overrides AppVersion=$(APP_VERSION)

update-cf-stack: package-cf-stack deploy-cf-stack ## [APP_VERSION][STACK] Package local template and deploy the cloudformation STACK.

delete-cf-stack: guard-STACK guard-DANGEROUS ## [STACK][DANGEROUS] Delete the cloudformation STACK. Requires DANGEROUS in env.
	@$(AWS) cloudformation delete-stack --stack-name $(STACK)

update-stack-app-version: guard-STACK guard-APP_VERSION ## [APP_VERSION][STACK] Update the cloudformation STACK using existing remote template.
	@$(AWS) cloudformation update-stack --stack-name $(STACK) \
		--use-previous-template \
		--parameters ParameterKey=AppVersion,ParameterValue=$(APP_VERSION)
	@echo "Waiting for APP_VERSION ${APP_VERSION} to deploy to STACK ${STACK}"
	@$(AWS) cloudformation wait stack-update-complete --stack-name $(STACK)

deploy: build-deploy-image push-image update-stack-app-version ## [APP_VERSION][STACK] Build and deploy a new APP_VERSION to STACK.

logs: guard-STACK ## [STACK] Tail CloudWatch logs for the STACK.
	@$(AWSLOGS) get -w /ecs/$(STACK)TaskDefinition

get-key-path: ## Get the path to the SSH key for the current AWS account and region
	@echo "~/.ssh/dioptra-bastion-$(AWS_DEFAULT_REGION)-$(AWS_ACCT_NUMBER)"

get-bastion-ip: guard-STACK ## [STACK] Get the IP of the stack's bastion host
	@echo $(BASTION_IP)

get-db-password: guard-STACK ## [STACK] Get the DB password for the current stack
	@$(AWS) ssm get-parameter --name '/$(STACK)/db-password' --output text --query 'Parameter.Value' | tr -d ' \r\n'

ssh-bastion: guard-STACK ## [STACK] SSH to the stack's bastion host
	@ssh -i ~/.ssh/dioptra-bastion-$(AWS_DEFAULT_REGION)-$(AWS_ACCT_NUMBER) ec2-user@$(BASTION_IP)

ssh-pg-tunnel-bastion: guard-STACK ## [STACK] Open a tunnel from local port 5433 to the DB cluster port 5432 via the bastion host
	@ssh -N -L 5433:$(RDS_DB_HOST):5432 -i ~/.ssh/dioptra-bastion-$(AWS_DEFAULT_REGION)-$(AWS_ACCT_NUMBER) ec2-user@$(BASTION_IP)

cfn-lint: ## Run the cfn-lint linter on the CloudFormation templates.
	@$(DOCKER_RUN_AWSCLI) cfn-lint cloudformation/*.yaml

guard-%:
	@if [ -z '${${*}}' ]; then echo 'ERROR: variable $* not set' && exit 1; fi
