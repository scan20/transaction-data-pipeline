FROM pypy:3.6-7.2.0

ARG REQUIREMENTS=base

ARG BUILD_SHA
ENV BUILD_SHA=${BUILD_SHA}

ARG BUILD_TIME
ENV BUILD_TIME=${BUILD_TIME}

WORKDIR /app
COPY requirements/* /app/
RUN pip3 install --upgrade pip
RUN pip3 install -r ${REQUIREMENTS}.txt
COPY dioptra /app/dioptra
COPY .flake8 /app/.flake8
CMD [ "pypy3", "-m", "dioptra.pipeline" ]
