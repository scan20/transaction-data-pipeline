---
AWSTemplateFormatVersion: 2010-09-09
Description: OMBU Dioptra / Fargate

Parameters:
  Name:
    Type: String
  S3Bucket:
    Type: String
  DBHost:
    Type: String
  DBName:
    Type: String
  DBUsername:
    Type: String
  DBPassword:
    Type: AWS::SSM::Parameter::Value<String>
  ECRRepository: 
    Type: String
  AppVersion:
    Type: String
  AppendMode:
    Type: String
  FromEmailAddress:
    Type: String
  MaxErrors:
    Type: Number
  SentryDsn:
    Type: String
  SESIdentityArn:
    Type: String
  SESRegion:
    Type: String

Resources:
  Cluster:
    Type: AWS::ECS::Cluster
    Properties:
      ClusterName: !Join ['', [!Ref Name, Cluster]]

  TaskDefinition:
    Type: AWS::ECS::TaskDefinition
    Properties:
      Family: !Join ['', [!Ref Name, TaskDefinition]]
      NetworkMode: awsvpc
      RequiresCompatibilities:
        - FARGATE
      # 256 (.25 vCPU) - Available memory values: 0.5GB, 1GB, 2GB
      # 512 (.5 vCPU) - Available memory values: 1GB, 2GB, 3GB, 4GB
      # 1024 (1 vCPU) - Available memory values: 2GB, 3GB, 4GB, 5GB, 6GB, 7GB, 8GB
      # 2048 (2 vCPU) - Available memory values: Between 4GB and 16GB in 1GB increments
      # 4096 (4 vCPU) - Available memory values: Between 8GB and 30GB in 1GB increments
      Cpu: "4096"
      # 0.5GB, 1GB, 2GB - Available cpu values: 256 (.25 vCPU)
      # 1GB, 2GB, 3GB, 4GB - Available cpu values: 512 (.5 vCPU)
      # 2GB, 3GB, 4GB, 5GB, 6GB, 7GB, 8GB - Available cpu values: 1024 (1 vCPU)
      # Between 4GB and 16GB in 1GB increments - Available cpu values: 2048 (2 vCPU)
      # Between 8GB and 30GB in 1GB increments - Available cpu values: 4096 (4 vCPU)
      Memory: 8GB
      ExecutionRoleArn: !Ref ExecutionRole
      TaskRoleArn: !Ref TaskRole
      ContainerDefinitions:
        - Name: !Ref Name
          Essential: true
          Image: !Sub '${ECRRepository}/dioptra/transaction-pipeline:${AppVersion}'
          Environment:
            - Name: APPEND_MODE
              Value: !Ref AppendMode
            - Name: AWS_REGION
              Value: !Ref AWS::Region
            - Name: S3_BUCKET
              Value: !Ref S3Bucket
            - Name: DATABASE_URL
              Value: !Sub 'postgres://${DBUsername}:${DBPassword}@${DBHost}:5432/${DBName}'
            - Name: MAX_VALIDATION_ERRORS
              Value: !Ref MaxErrors
            - Name: FROM_EMAIL
              Value: !Ref FromEmailAddress
            - Name: SENTRY_DSN
              Value: !Ref SentryDsn
            - Name: SES_REGION
              Value: !Ref SESRegion

          # Config we want to set at runtime, without requiring a redeploy.
          # See appconfig Config variables for what is supported.
          # Anything you place here MUST be set via SSM (make put-ssm-param).
          #  Secrets:
          #   - Name: CHUNK_SIZE
          #     ValueFrom: !Sub 'arn:aws:ssm:${AWS::Region}:${AWS::AccountId}:parameter/${Name}/CHUNK_SIZE'
          LogConfiguration:
            LogDriver: awslogs
            Options:
              awslogs-region: !Ref AWS::Region
              awslogs-group: !Ref LogGroup
              awslogs-stream-prefix: ecs

  # Allows the ECS worker to get images from ECR, write logs to Cloudwatch, and read SSM params.
  ExecutionRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Statement:
          - Effect: Allow
            Principal:
              Service: ecs-tasks.amazonaws.com
            Action: 'sts:AssumeRole'
      ManagedPolicyArns:
        - 'arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy'
      Policies:
        - PolicyName: !Sub 'SSMParamAccess_${Name}'
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Effect: Allow
                Action:
                  - 'ssm:GetParameters'
                Resource: !Sub 'arn:aws:ssm:${AWS::Region}:${AWS::AccountId}:parameter/${Name}/*'

  # Task container runs with this role. Needs access to the S3 bucket.
  TaskRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Statement:
          - Effect: Allow
            Principal:
              Service: ecs-tasks.amazonaws.com
            Action: 'sts:AssumeRole'
      Policies:
        - PolicyName: !Sub 'S3BucketAccess_${Name}'
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Effect: Allow
                Action:
                  - 's3:ListBucket'
                Resource: !Sub 'arn:aws:s3:::${S3Bucket}'
              - Effect: Allow
                Action:
                  - 's3:GetObject'
                  - 's3:DeleteObject'
                Resource: !Sub 'arn:aws:s3:::${S3Bucket}/*'
        - PolicyName: !Sub 'SendEmail_${Name}'
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Effect: Allow
                Action:
                  - 'ses:SendEmail'
                Resource: !Ref SESIdentityArn

  LogGroup:
    Type: AWS::Logs::LogGroup
    Properties:
      LogGroupName: !Join ['', [/ecs/, !Ref Name, TaskDefinition]]

Outputs:
  TaskDefinitionArn:
    Value: !Ref TaskDefinition
  ClusterArn:
    Value: !GetAtt Cluster.Arn
